import { Select } from "antd";
import { Chart } from "react-google-charts";
import RenderChart from "./RenderChart";
const ClientStatics = () => {
  const dataRenderChart = [
    {
      chartType: "Bar",
      urlData: 'statics/client/job',
      optionsView: (
       <></>
      ),
      dataOptionView: {},
      options: {
        chart: {
          title: "Thống kê số công việc đã đăng",
          vAxis: {
            title: "Số lượng",
            viewWindow: { min: 0 }, // Giới hạn trục Y từ 0
            format: '0', // Đảm bảo chỉ hiển thị số nguyên
          },
          legend: { position: "none" },
        },
      },
    },
    {
      chartType: "Bar",
      urlData: 'statics/client/bids',
      optionsView: (
       <></>
      ),
      dataOptionView: {},
      options: {
        chart: {
          title: "Thống kê chi trong các năm",
        },
      },
    },
    {
      chartType: "Bar",
      urlData: 'statics/client/status',
      optionsView: (
       <></>
      ),
      dataOptionView: {},
      options: {
        chart: {
          title: "Thống kê trạng thái công việc trong 12 tháng gần nhất",
        },
      },
    },
    {
      chartType: "PieChart",
      urlData: 'statics/client/fb',
      optionsView: (
        <></>
      ),
      dataOptionView: {},
      options: {
        
          title: "Thống kê số lượt đánh giá",
          is3D: true,
        
      },
    }
  ];
  return (
    <div style={{ marginTop: 150 }}>
      {dataRenderChart.map((data: any) => {
        return (
          <>
            <RenderChart
              typeChart={data.chartType}
              optionsView={() => data.optionsView}
              options={data.options}
              urlData={data.urlData}
              dataOptionsView={data.dataOptionsView}
            ></RenderChart>
          </>
        );
      })}
    </div>
  );
};
export default ClientStatics;
