import ReportForm from "@/app/(protected)/components/ReportForm";
import { Skeleton } from "@/app/components/ui/skeleton";
import { AuthContext } from "@/app/providers/AuthProvider";
import { NotificationContext } from "@/app/providers/NotificationProvider";
import { clientServices } from "@/app/services/client.services";
import { commonServices } from "@/app/services/common.services";
import { FreelancerPreviewInfo, Rate } from "@/app/types/authentication.types";
import { ReportType } from "@/app/types/reports.type";
import { Button, Image } from "antd";
import { format } from "date-fns";
import Cookies from "js-cookie";
import { Pencil } from "lucide-react";
import { useRouter } from "next/navigation";
import { useContext, useEffect, useState } from "react";
import { IoMdMore } from "react-icons/io";
import JobTable from "./JobTable";

interface IDetailUserInfo {
  userId: string;
  userType: string;
}

const DetailUserInfo: React.FC<IDetailUserInfo> = ({ userId, userType }) => {
  const [loading, setLoading] = useState(false);
  const [freelancerInfo, setFreelancerInfo] =
    useState<FreelancerPreviewInfo | null>(null);
  const router = useRouter();
  const { user } = useContext(AuthContext);
  const [accountType, setAccountType] = useState("");
  const [isMyProfile, setIsMyProfile] = useState(true);
  const { openNotificationWithIcon } = useContext(NotificationContext);

  useEffect(() => {
    const accountType = Cookies.get("account_type");
    setAccountType(accountType || "");
  }, []);

  useEffect(() => {
    const fetchfreelancerInfoData = async (
      userType: string,
      freelancerInfoId: string
    ) => {
      try {
        setLoading(true);
        const res = await clientServices.getDetailInfo(userType, userId);
        if (res.data) {
          setFreelancerInfo(res.data);
        }
      } catch (error) {
      } finally {
        setLoading(false);
      }
    };
    if (userId) {
      fetchfreelancerInfoData(userType, userId);
    }
  }, [userType, userId]);

  useEffect(() => {
    if (
      user &&
      freelancerInfo &&
      freelancerInfo.base_info &&
      accountType != userType
    ) {
      if (user.id != freelancerInfo.base_info.id) setIsMyProfile(false);
      else setIsMyProfile(true);
    }
  }, [user, freelancerInfo]);

  const [isOpenModel, setOpenModal] = useState(false);

  const handleReport = async (content: string, file: File | undefined) => {
    if (!loading && user && freelancerInfo && freelancerInfo.base_info) {
      try {
        const formData = new FormData();
        if (file) formData.append("content_file", file);
        formData.append(
          "client_id",
          accountType == "client"
            ? user.id.toString()
            : freelancerInfo.base_info.id.toString()
        );
        formData.append(
          "freelancer_id",
          accountType == "freelancer"
            ? user.id.toString()
            : freelancerInfo.base_info.id.toString()
        );
        formData.append("content", content);
        formData.append(
          "type_id",
          accountType == "freelancer"
            ? ReportType.Client.toString()
            : ReportType.Freelancer.toString()
        );

        const res = await commonServices.report(formData);
        if (res.status == 200) {
          openNotificationWithIcon(
            "success",
            "Thành công",
            "Báo cáo thành công"
          );
        } else openNotificationWithIcon("error", "Lỗi", res.message);
      } catch (error) {
        openNotificationWithIcon("error", "Lỗi", error);
      }
    }
  };

  if (freelancerInfo && !freelancerInfo.base_info) {
    return (
      <div className="h-full w-full ">
        <div className=" h-full w-full flex items-center justify-center">
          <h2 className="text-lg">Không tìm thấy dữ liệu</h2>
        </div>
      </div>
    );
  }

  return (
    <>
      <section className="px-20">
        <div className="flex">
          <h2 className="text-4xl font-semibold -tracking-[1px] flex-1">
            Thông tin ứng viên
          </h2>
          {!isMyProfile && (
            <div className="p-2 group z-50 relative">
              <IoMdMore />
              <div className="hidden group-hover:block absolute shadow-xl rounded-md px-4 py-1 top-6 left-2 w-[120px] border">
                <div
                  className="text-[15px] cursor-pointer hover:text-blue-500 pt-1 pb-1"
                  onClick={() => {
                    setOpenModal(true);
                  }}
                >
                  Báo cáo
                </div>
              </div>
            </div>
          )}
        </div>
        <div className="my-8 border border-solid border-[#d5e0d5] rounded-[16px]">
          <article className="mb-8">
            <header className="p-8 border-b border-solid border-[#d5e0d5] flex items-center justify-between">
              {loading ? (
                <Skeleton className="w-full h-7" />
              ) : (
                <>
                  <h3 className="text-2xl font-medium">Thông tin liên lạc</h3>
                </>
              )}
              {parseInt(userId) == user?.id && userType == accountType ? (
                <Button
                  onClick={() => {
                    router.replace("/profile");
                  }}
                  className="bg-transparent hover:bg-transparent border-2 border-solid border-primary-color rounded-full px-1.5"
                >
                  <Pencil fill="#108a00" />
                </Button>
              ) : (
                <></>
              )}
            </header>
            <div className="flex items-center">
              <div className="flex justify-center items-center mr-4 w-[30%]">
                <Image
                  src={
                    freelancerInfo?.base_info?.avatar_url ||
                    "/images/others/unknown_avatar.png"
                  }
                  alt="image"
                  className="w-[150px] h-[150px] rounded-full"
                  width={150}
                  height={150}
                />
              </div>
              <div className="flex-1">
                <div className="pt-8 mb-5">
                  {loading ? (
                    <Skeleton className="w-full h-8" />
                  ) : (
                    <h3 className="text-xl font-medium">
                      {`${freelancerInfo?.base_info.last_name} ${freelancerInfo?.base_info.first_name}`}
                    </h3>
                  )}
                </div>
                <div className="mb-2">
                  <div className="flex items-center">
                    {loading ? (
                      <Skeleton className="w-full h-8" />
                    ) : (
                      <>
                        <h3 className="text-lg font-medium">Ngày sinh:</h3>
                        <p className="ml-4">
                          {freelancerInfo?.base_info.date_of_birth &&
                            format(
                              freelancerInfo?.base_info.date_of_birth,
                              "dd-M-yyyy"
                            )}
                        </p>
                      </>
                    )}
                  </div>
                </div>
                <div className="mb-2">
                  <div className="flex items-center">
                    {loading ? (
                      <Skeleton className="w-full h-8" />
                    ) : (
                      <>
                        <h3 className="text-lg font-medium">Giới tính:</h3>
                        <p className="ml-4">
                          {freelancerInfo?.base_info.sex === "0" ? "Nam" : "Nữ"}
                        </p>
                      </>
                    )}
                  </div>
                </div>
                <div className="mb-2">
                  <div className="flex items-center">
                    {loading ? (
                      <Skeleton className="w-full h-8" />
                    ) : (
                      <>
                        <h3 className="text-lg font-medium">Số điện thoại:</h3>
                        <p className="ml-4">
                          {freelancerInfo?.base_info?.phone_num}
                        </p>
                      </>
                    )}
                  </div>
                </div>
                <div className="mb-2">
                  <div className="flex items-center">
                    {loading ? (
                      <Skeleton className="w-full h-8" />
                    ) : (
                      <>
                        <h3 className="text-lg font-medium">Email:</h3>
                        <p className="ml-4">
                          {freelancerInfo?.base_info?.email}
                        </p>
                      </>
                    )}
                  </div>
                </div>
                <div className="mb-2">
                  <div className="flex items-center">
                    {loading ? (
                      <Skeleton className="w-full h-8" />
                    ) : (
                      <>
                        <h3 className="text-lg font-medium">Địa chỉ:</h3>
                        <p className="ml-4">
                          {freelancerInfo?.base_info?.address}
                        </p>
                      </>
                    )}
                  </div>
                </div>
                {/* <div className='mb-2'>
                                <div className='flex items-center'>
                                    {loading ? (
                                        <Skeleton className='w-full h-8' />
                                    ) : (
                                        <>
                                            <h3 className='text-lg font-medium'>
                                                Giới thiệu ngắn:
                                            </h3>
                                            <p className='ml-4'>
                                                {
                                                    freelancerInfo?.base_info
                                                        ?.intro
                                                }
                                            </p>
                                        </>
                                    )}
                                </div>
                            </div> */}
                {userType == "freelancer" ? (
                  <div className="mb-2">
                    <div className="flex items-center">
                      {loading ? (
                        <Skeleton className="w-full h-8" />
                      ) : (
                        <>
                          <h3 className="text-lg font-medium">Kỹ năng:</h3>
                          <div className="ml-4 flex items-center gap-x-3">
                            {freelancerInfo?.experience.map((s) => (
                              <div
                                key={`selected-skill-${s.id}`}
                                className="cursor-pointer flex items-center gap-x-1 border-2 border-solid border-transparent px-3 rounded-2xl h-8 text-sm font-medium leading-[31px] bg-[#108a00] hover:bg-[#14a800] text-white"
                              >
                                {s.name}
                              </div>
                            ))}
                          </div>
                        </>
                      )}
                    </div>
                  </div>
                ) : (
                  <></>
                )}
              </div>
            </div>
          </article>
        </div>
        {accountType === "admin" && (
          <div className="my-8 border border-solid border-[#d5e0d5] rounded-[16px]">
            <article className="mb-8">
              <header className="p-8 border-b border-solid border-[#d5e0d5] flex items-center justify-between">
                {loading ? (
                  <Skeleton className="w-full h-7" />
                ) : (
                  <>
                    <h3 className="text-2xl font-medium">Thông tin xác minh</h3>
                  </>
                )}
              </header>
              <div style={{ padding: 20 }}>
                {freelancerInfo?.base_info.is_completed_profile == 0 ||
                !freelancerInfo?.base_info ? (
                  <p>Tài khoản chưa được xác minh</p>
                ) : (
                  <div>
                    <p className="pb-5">
                      Số căn cước công dân:{" "}
                      <span className="font-bold">
                        {freelancerInfo?.base_info.citizen_identification_id}
                      </span>
                    </p>
                    <Image
                      src={freelancerInfo?.base_info.citizen_identification_url}
                      width={500}
                    />
                  </div>
                )}
              </div>
            </article>
          </div>
        )}
        <div className="my-8 border border-solid border-[#d5e0d5] rounded-[16px]">
          <article className="mb-8">
            <header className="p-8 border-b border-solid border-[#d5e0d5] flex items-center justify-between">
              {loading ? (
                <Skeleton className="w-full h-7" />
              ) : (
                <>
                  <h3 className="text-2xl font-medium">Giới Thiệu</h3>
                </>
              )}
            </header>
            <div
              style={{ padding: 20 }}
              dangerouslySetInnerHTML={{
                __html:
                  userType == "client"
                    ? freelancerInfo?.base_info?.introduce || ""
                    : freelancerInfo?.base_info?.intro || "",
              }}
            />
          </article>
        </div>
        <div className="my-8 border border-solid border-[#d5e0d5] rounded-[16px]">
          <article className="mb-8">
            <header className="p-8 border-b border-solid border-[#d5e0d5] flex items-center justify-between">
              {loading ? (
                <Skeleton className="w-full h-7" />
              ) : (
                <>
                  <h3 className="text-2xl font-medium">Những đánh giá</h3>
                </>
              )}
            </header>
            <div className="p-8">
              <div>
                {freelancerInfo?.rate_recent.map(
                  (rate: Rate, index: number) => (
                    <div className="mb-2" key={index}>
                      <span>
                        {rate.username} đã đánh giá <strong>{rate.rate}</strong>{" "}
                        sao: {""}
                        {rate.comment}
                      </span>
                    </div>
                  )
                )}
              </div>
            </div>
          </article>
        </div>

        <div className="my-8 border border-solid border-[#d5e0d5] rounded-[16px]">
          <article className="mb-8">
            <header className="p-8 border-b border-solid border-[#d5e0d5] flex items-center justify-between">
              {loading ? (
                <Skeleton className="w-full h-7" />
              ) : (
                <>
                  <h3 className="text-2xl font-medium">
                    {userType == "client"
                      ? "Công việc đã đăng"
                      : "Công việc đã thực hiện"}
                  </h3>
                </>
              )}
            </header>
            <JobTable job={freelancerInfo?.job || []} />
          </article>
        </div>
        <ReportForm
          isOpen={isOpenModel}
          onCancel={() => {
            setOpenModal(false);
          }}
          onReport={handleReport}
        />
      </section>
    </>
  );
};

export default DetailUserInfo;
