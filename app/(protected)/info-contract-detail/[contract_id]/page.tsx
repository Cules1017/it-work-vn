"use client";
import AppThirdwebProvider from "@/app/providers/ThirdwebProvider";
import { useStateContext } from "@/context";
import { useContract } from "@thirdweb-dev/react";
import { useContext, useEffect, useState } from "react";
import PizZip from "pizzip";
import Docxtemplater from "docxtemplater";
import SignaturePad from "./SignaturePad";
//import ImageModule from "docxtemplater-image-module";
import { commonServices } from "@/app/services/common.services";
import FreelancerInfo from "@/app/client/show-freelancer-info/[freelancer_id]/page";
import { Modal, Button, Tooltip, Spin, notification, Checkbox, Input } from "antd";
import { DetailClientPost } from "@/app/types/freelancer.type";
import { BaseInfo } from "@/app/types/authentication.types";
import InputOtp from "@/app/client/post/[id]/components/InputOtp";
import PolicyViews from "@/app/client/post/[id]/components/PolicyViews";
import SignaturePadSimple from "@/app/client/post/[id]/components/SignaturePad";
import { ReloadIcon } from "@radix-ui/react-icons";
import Cookies from "js-cookie";
import { appConfig } from "@/app/configs/app.config";
import { NotificationContext } from "@/app/providers/NotificationProvider";
import { smartContractService } from "@/app/services/thirbWeb.services";
import { freelancerServices } from "@/app/services/freelancer.services";
import { uploadBase64Image } from "@/app/utils/helpers";
import { includes } from "lodash";
import { useRouter } from "next/navigation";
import { Textarea } from "@/app/components/ui/textarea";
import { BigNumber } from 'ethers';

interface IContractDetail {
  params: {
    contract_id: string;
  };
}

const ContractDetail: React.FC<IContractDetail> = ({ params }) => {
  //console.log("id", params.job_id);
  const [loading, setLoading] = useState(false);
  const [checked, setChecked] = useState(false);
  const [disabledPolicy, setDisabledPolicy] = useState(true);
  const [verify, setVerify] = useState(false);
  const [contractInfo, setContractInfo] = useState({
    contract_id: -1,
    title: "",
    description: "",
    signatureF: "",
    signatureC: "",
    bids: "0",
    status: 0,
    address_client: 0,
    address_freelancer: 0,
    freelancerId: 0,
    clientId: 0,
    cancelReason:''
  });
  const { address, connect } = useStateContext();
  const [reload, setReload] = useState(false);
  const [accountType, setAccountType] = useState("");

  useEffect(() => {
    const accountType = Cookies.get("account_type");
    setAccountType(accountType || "");
  }, []);

  const { contract } = useContract(appConfig.contractId);
  const [contractFile, setContractFile] = useState<string | null>(null);
  const [imgSignature, setImgSignature] = useState<string | null>(null);
  let { openNotificationWithIcon } = useContext(NotificationContext);
  const router = useRouter();

  const getDataContract = async () => {
    try {
      setLoading(true);
      const data = await smartContractService.getContractDetailByIndex(
        parseInt(params.contract_id),
        contract
      );

      console.log("DATA 11", data);
      if (data) {
        setContractInfo(data);
      }
      setLoading(false);
      if (data == null && contract) {
        openNotificationWithIcon(
          "error",
          "Tải Thất Bại",
          "Hợp đồng không tồn tại vui lòng trở lại trang chủ"
        );
       // router.replace(`/${accountType}/dashboard`);
      }
    } catch (error) {
      setLoading(false);
      openNotificationWithIcon(
        "error",
        "Tải Thất Bại",
        "Dữ liệu đang bị bất đồng bộ vui lòng trở lại trang chủ"
      );
      console.log(error);
    }
  };

  /////Các hàm MODAL/////
  const [open, setOpen] = useState(false);

  const hideModal = () => {
    setOpen(false);
  };

  ///////////////////////
  const onChangeCheckBox = (e: any) => {
    console.log("checked = ", e.target.checked);
    setChecked(e.target.checked);
  };
  const base64Regex = /^data:image\/(png|jpg|svg|svg\+xml);base64,/;
  const validBase64 =
    /^(?:[A-Za-z0-9+/]{4})*(?:[A-Za-z0-9+/]{2}==|[A-Za-z0-9+/]{3}=)?$/;
  const base64Parser = (dataURL: string) => {
    if (typeof dataURL !== "string" || !base64Regex.test(dataURL)) {
      return false;
    }

    const stringBase64 = dataURL.replace(base64Regex, "");

    if (!validBase64.test(stringBase64)) {
      throw new Error(
        "Error parsing base64 data, your data contains invalid characters"
      );
    }

    // For nodejs, return a Buffer
    if (typeof Buffer !== "undefined" && Buffer.from) {
      return Buffer.from(stringBase64, "base64");
    }

    // For browsers, return a string (of binary content) :
    const binaryString = window.atob(stringBase64);
    const len = binaryString.length;
    const bytes = new Uint8Array(len);
    for (let i = 0; i < len; i++) {
      const ascii = binaryString.charCodeAt(i);
      bytes[i] = ascii;
    }
    return bytes.buffer;
  };

  console.log("contract information", contractInfo);
  useEffect(() => {
    getDataContract();
  }, [contract, reload]);

  ///LOad Template
  const loadFile = (url: string, callback: any) => {
    var xhr = new XMLHttpRequest();
    xhr.open("GET", url, true);
    xhr.responseType = "arraybuffer";
    xhr.onload = function () {
      var data = new Uint8Array(xhr.response);
      var arr = new Array();
      for (var i = 0; i !== data.length; ++i)
        arr[i] = String.fromCharCode(data[i]);
      callback(arr.join(""));
    };
    xhr.send();
  };
  const xuathd = async () => {
    console.log("haha");

    const reader = new FileReader();
    const filePath = "/tmp/mauhd.doc"; // Assuming the file input name is 'file'
    const response = await fetch(filePath);
    const arrayBuffer = await response.arrayBuffer();

    // Convert array buffer to Uint8Array
    const uint8Array = new Uint8Array(arrayBuffer);
    const data = {
      data: JSON.stringify({
        image:
          "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKCAIAAAACUFjqAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH4QIJBywfp3IOswAAAB1pVFh0Q29tbWVudAAAAAAAQ3JlYXRlZCB3aXRoIEdJTVBkLmUHAAAAkUlEQVQY052PMQqDQBREZ1f/d1kUm3SxkeAF/FdIjpOcw2vpKcRWCwsRPMFPsaIQSIoMr5pXDGNUFd9j8TOn7kRW71fvO5HTq6qqtnWtzh20IqE3YXtL0zyKwAROQLQ5l/c9gHjfKK6wMZjADE6s49Dver4/smEAc2CuqgwAYI5jU9NcxhHEy60sni986H9+vwG1yDHfK1jitgAAAABJRU5ErkJggg==", // Dữ liệu base64 của hình ảnh
      }),
      doc: uint8Array,
    };
    console.log("Data_>", data);

    await commonServices.submitTemplates(
      "https://docxapi.javascript-ninja.fr/api/v1/generate?ignore_unknown_tags=false&extension=doc&silent=true&imagesize=100x100&delimiters={+}&subrender=false&subtemplateSubsections=false",
      data
    );
    //await commonServices.getFileTemplate('https://docxapi.javascript-ninja.fr/api/v1/last')
    window.open("https://docxapi.javascript-ninja.fr/api/v1/last", "_blank");
  };
  // Fill data into the template and save as a Word file
  const generateDocument = () => {
    // Đường dẫn tới tệp mẫu DOCX
    const templateFilePath = "/tmp/mauhd.docx";

    // Load nội dung của tệp mẫu
    const xhr = new XMLHttpRequest();
    xhr.open("GET", templateFilePath, true);
    xhr.responseType = "arraybuffer";
    const imageData = {
      base64:
        "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKCAIAAAACUFjqAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH4QIJBywfp3IOswAAAB1pVFh0Q29tbWVudAAAAAAAQ3JlYXRlZCB3aXRoIEdJTVBkLmUHAAAAkUlEQVQY052PMQqDQBREZ1f/d1kUm3SxkeAF/FdIjpOcw2vpKcRWCwsRPMFPsaIQSIoMr5pXDGNUFd9j8TOn7kRW71fvO5HTq6qqtnWtzh20IqE3YXtL0zyKwAROQLQ5l/c9gHjfKK6wMZjADE6s49Dver4/smEAc2CuqgwAYI5jU9NcxhHEy60sni986H9+vwG1yDHfK1jitgAAAABJRU5ErkJggg==", // Dữ liệu base64 của hình ảnh
      width: 200, // Độ rộng của hình ảnh (đơn vị pixel)
      height: 100, // Độ cao của hình ảnh (đơn vị pixel)
    };
    // Chuyển đổi hình ảnh base64 thành ArrayBuffer
    const base64ToArrayBuffer = (base64: string) => {
      const binaryString = window.atob(base64.split(",")[1]);
      const len = binaryString.length;
      const bytes = new Uint8Array(len);
      for (let i = 0; i < len; i++) {
        bytes[i] = binaryString.charCodeAt(i);
      }
      return bytes.buffer;
    };
    xhr.onload = function () {
      const data = new Uint8Array(xhr.response);
      const zip = new PizZip(data);
      let doc;
      try {
        doc = new Docxtemplater(zip);
        console.log("doc", doc);
      } catch (error) {
        console.log("erroooo_>", error);
        return;
      }

      const dateObject = new Date(job.nominee.updated_at);

      const year = dateObject.getFullYear();
      // Lấy tháng (trả về giá trị từ 0 - 11, với 0 là tháng 1)
      const month = dateObject.getMonth() + 1;
      // Lấy ngày trong tháng
      const day = dateObject.getDate();
      // Thiết lập dữ liệu cho mẫu
      console.log("jo", client);

      doc.setData({
        ngay: day,
        thang: month,
        nam: year,
        dia_diem: "Trang Web Tìm Việc IT Hot Nhất Việt Nam",
        company_name: client?.company_name ?? "",
        client_address: client?.address ?? "",
        client_numphone: client?.phone_num ?? "",
        client_email: client?.email ?? "",
        client_name: (client?.first_name ?? "") + (client?.last_name ?? ""),
        description: contractInfo?.description ?? "",
        freelancer_address: freelancer?.address ?? "",
        freelancer_email: freelancer?.email ?? "",
        freelancer_numphone: freelancer?.phone_num ?? "",
        freelancer_dob: freelancer?.date_of_birth ?? "",
        freelancer_name:
          (freelancer?.first_name ?? "") + (freelancer?.last_name ?? ""),

        image: appConfig.aws3 + contractInfo.signatureC,
        // Thêm các dữ liệu khác tương ứng với mẫu của bạn
      });

      try {
        // Render dữ liệu vào tệp mẫu
        doc.render();
      } catch (error) {
        console.log("rendr", error);
        return;
      }

      // Lấy nội dung đã render
      const out = doc.getZip().generate({
        type: "blob",
        mimeType:
          "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
      });
      console.log("==>", out);
      setContractFile(URL.createObjectURL(out));
    };
    xhr.send(null);
  };

  useEffect(() => {
    if (contractFile !== null) {
      window.open(contractFile, "_blank");
    }
  }, [contractFile]);
  const [client, setClient] = useState<BaseInfo | null>(null);
  const [freelancer, setFreelancer] = useState<BaseInfo | null>(null);
  const [job, setJob] = useState<DetailClientPost | null | any>(null);

  const setInfoUserContract = async () => {
    try {
      // const infoJob = (await commonServices.getInfoJob(parseInt(params.)))
      //   .data;
      // setJob(infoJob);
      console.log("info job", contractInfo.clientId);
      const infoClient = (
        await commonServices.getInfoUser({
          id: BigNumber.isBigNumber(contractInfo.clientId)
          ? contractInfo.clientId.toNumber()
          : contractInfo.clientId,
          type: "client",
        })
      ).data.base_info;
      setClient(infoClient);
      const freeLancerInfo = (
        await commonServices.getInfoUser({
          id: BigNumber.isBigNumber(contractInfo.freelancerId)
          ? contractInfo.freelancerId.toNumber()
          : contractInfo.freelancerId,
          type: "freelancer",
        })
      ).data.base_info;
      setFreelancer(freeLancerInfo);
      console.log(freeLancerInfo);
    } catch (error) {
      setLoading(false);
      console.log("có lỗi khi thực hiện dữ liệu chưa đồng bộ", error);
    }
  };
  useEffect(() => {
    if (contractInfo.contract_id >= 0) {
      setLoading(true);
      setInfoUserContract();
      //Bắt đầu gọi lấy thông tin Freelancer
      setLoading(false);
      //Bắt đầu gọi lấy thông tin Client
    }
    if (contractInfo.status >= 0) {
      setChecked(true);
    }
  }, [contractInfo]);
  const [signLink, setSignLink] = useState<any>("");
  const checkSign = async () => {
    console.log("imgSignature", imgSignature);
    setLoading(true);
    const rss = await uploadBase64Image(imgSignature, "/upload-file");
    console.log("imgSignature-->chuyển", rss);
    setLoading(false);
    if (rss) {
      setSignLink(rss);
      if (!verify) {
        setOpen(true);
      } else {
        setOpen(false);
      }
    }
  };

  useEffect(() => {
    if (imgSignature) {
      checkSign();
    }
  }, [imgSignature, verify]);
  const callAceptContract = async () => {
    try {
      const responseContract = await contract?.call("acceptContract", [
        contractInfo.contract_id,
        signLink,
      ]);
      if (responseContract) {
        const resConfirm =
          await freelancerServices.confirmJobAfterFreelancerSignContract(
            job.id
          );
        setReload(!reload);
        commonServices.sendNotication({
          title: `Hợp đồng ${job.title} của bạn đã được freelancer chấp thuận`,
          message: `Ứng viên ${freelancer?.last_name} đã chấp thuận công việc ${job.title} của bạn. Vui lòng ấn để xem hợp đồng.`,
          linkable: `/info-contract/${job.id}`,
          smail: 1,
          imagefile: null,
          user_type: "client", //type cua nguoi nhan
          user_id: client?.id, // id cua nguoi nhan
        });
        openNotificationWithIcon(
          "success",
          "Ký Hợp Đồng Thành Công",
          "Hợp đồng của bạn đã được kí thành công bạn có thể xem lại."
        );
      } else {
        openNotificationWithIcon(
          "error",
          "Ký Hợp Đồng Thất Bại",
          "Có lỗi xảy ra vui lòng thử lại sau."
        );
      }
    } catch (error) {
      openNotificationWithIcon(
        "error",
        "Ký Hợp Đồng Thất Bại",
        "Có lỗi xảy ra vui lòng thử lại sau."
      );
    } finally {
      setLoading(false);
    }
  };

  const onSubmit = async () => {
    setLoading(true);
    callAceptContract();
  };
  const [reasonCancel, setReasonCancel] = useState();
  const [cancelContracts, setCancelContracts] = useState(false);

  return (
    <div style={{ height: "auto", minHeight: "1700px", display: "block" }}>
     <Spin spinning={loading} fullscreen tip="Đang tải...."></Spin>
      <div
        style={{
          display: "flex",
          minHeight: "fit-content",
          flexDirection: "column",
        }}
      >
        <div
          style={{
            width: "100%",
            minHeight: "fit-content",
            backgroundImage:
              'url("https://t4.ftcdn.net/jpg/02/32/92/55/360_F_232925587_st4gM8b3TJHtjjddCIUNyVyFJmZqMmn4.jpg")',
            backgroundRepeat: "no-repeat",
            backgroundSize: "cover",
            display: "flex",
            justifyContent: "center",
          }}
        >
          {" "}
          <span
            style={{
              marginTop: 35,
              color: "yellow",
              fontSize: "35px",
              fontFamily: "roboto",
              fontWeight: 800,
            }}
          >
            Thông Tin Hợp Đồng{" "}
          </span>
        </div>
        <div
          style={{
            width: "100%",
            height: 100,
            display: "flex",
            justifyContent: "center",
          }}
        >
          <div
            style={{
              width: 1000,
              display: "inline-table",
              backgroundImage:
                'url("https://img.freepik.com/free-photo/watercolor-paper-texture_1194-5417.jpg")',
              backgroundRepeat: "no-repeat",
              backgroundSize: "cover",
              padding: "70px",
              fontFamily: "roboto",
              fontSize: 25,
            }}
          >
            <p className=" mb-4">
              <span className="font-semibold">Tên hợp đồng:</span>
              {" " + contractInfo.title}
            </p>
            <p className=" mb-4">
              <span className="font-semibold">Điều khoản hợp đồng:</span>
              {" " + contractInfo.description}
            </p>
            <p className=" mb-4">
              <span className="font-semibold">Giá thầu:</span>
              {" " + contractInfo.bids + " wei"}
            </p>
            <div className=" mb-4">
              <span className="font-semibold">Trạng thái:</span>
              <strong
                style={{
                  color: [1, 3, 4].includes(contractInfo.status)
                    ? "green"
                    : "red",
                }}
              >
                {contractInfo.status === 0
                  ? "Đang chờ ký"
                  : contractInfo.status === 1
                  ? "Đã ký"
                  : contractInfo.status === 2
                  ? "Đang chờ xác nhận hoàn thành"
                  : contractInfo.status === 3
                  ? "Đã hoàn thành"
                  : contractInfo.status === 4
                  ? "Hợp đồng đã bị huỷ bởi freelancer"
                  : "Hợp đồng đã bị huỷ bởi client"}
              </strong>
            </div>
            {contractInfo.status>=4?<p className=" mb-4">
              <span className="font-semibold">Lý do hủy:</span>
              {" " + contractInfo.cancelReason}
            </p>:<></>}
            <p className=" mb-4">
              <div className="font-semibold">Thông tin người thuê:</div>
              <p className="ml-9 mb-4">
                <span className="font-semibold">Họ tên: </span>
                {client ? client.first_name + client.last_name : ""}
              </p>
              <p className="ml-9 mb-4">
                <span className="font-semibold">Số Điện Thoại: </span>
                {client ? client.phone_num : ""}
              </p>
              <p className="ml-9 mb-4">
                <span className="font-semibold">Email: </span>
                {client ? client.email : ""}
              </p>
              <p className="ml-9 mb-4">
                <span className="font-semibold">Địa chỉ: </span>
                {client ? client.address : ""}
              </p>
            </p>
            <p className=" mb-4">
              <span className="font-semibold">Thông tin người nhận việc:</span>
              <p className="ml-9 mb-4">
                <span className="font-semibold">Họ tên: </span>
                {freelancer ? freelancer.first_name + freelancer.last_name : ""}
              </p>
              <p className="ml-9 mb-4">
                <span className="font-semibold">Số Điện Thoại: </span>
                {freelancer ? freelancer.phone_num : ""}
              </p>
              <p className="ml-9 mb-4">
                <span className="font-semibold">Email: </span>
                {freelancer ? freelancer.email : ""}
              </p>
              <p className="ml-9 mb-4">
                <span className="font-semibold">Địa chỉ: </span>
                {freelancer ? freelancer.address : ""}
              </p>
            </p>
            <div style={{ fontStyle: "italic", marginBottom: 9 }}>
              Cam kết thông tin trên mà tôi đã khai là đúng sự thật nếu sai tôi
              xin chịu hoàn toàn trách nhiệm trước pháp luật, và tôi đã đọc kỹ
              nội dung hợp đồng trước khi kí.
            </div>
            <div style={{ display: "flex", justifyContent: "space-between" }}>
              <p className="ml-9 mb-4">
                <span className="font-semibold">Chữ Kí Bên Thuê: </span>
                {contractInfo.signatureC != "" ? (
                  <img
                    style={{ height: 100 }}
                    src={appConfig.aws3 + contractInfo.signatureC}
                  />
                ) : (
                  <div style={{ height: 100 }}></div>
                )}
                <div
                  style={{
                    display: "flex",
                    width: "100%",
                    justifyContent: "center",
                  }}
                >
                  <span>
                    {client ? client.first_name + " " + client.last_name : ""}
                  </span>
                </div>
              </p>
              <p className="ml-9 mb-4">
                <span className="font-semibold">Chữ Kí Bên Nhận việc: </span>
                {contractInfo.signatureF != "" ? (
                  <img
                    style={{ height: 100 }}
                    src={appConfig.aws3 + contractInfo.signatureF}
                  />
                ) : (
                  <div style={{ height: 100 }}></div>
                )}
                <div
                  style={{
                    display: "flex",
                    width: "100%",
                    justifyContent: "center",
                  }}
                >
                  <span>
                    {freelancer
                      ? freelancer.first_name + " " + freelancer.last_name
                      : ""}
                  </span>
                </div>
              </p>
            </div>
            {/* {contractFile !== null ? (
        <Document documents={[{ uri: contractFile }]} />
      ) : (
        "" 
            )}*/}
            <div className="my-2">
              <PolicyViews setDisabledPolicy={setDisabledPolicy}></PolicyViews>
            </div>

            {accountType == "freelancer" && contractInfo.status <= 0 ? (
              <Checkbox
                checked={checked}
                disabled={disabledPolicy}
                onChange={onChangeCheckBox}
              >
                {disabledPolicy
                  ? "Vui lòng đọc điều khoảng trước khi tích vào đây"
                  : "Tôi đã đọc kỹ và tôi chấp nhận tất cả điều khoản nêu trên."}
              </Checkbox>
            ) : (
              <></>
            )}

            <div style={{ display: "flex", justifyContent: "end" }}>
            {contractInfo && contractInfo.status==0 && freelancer ? (
                <Button
                  onClick={() => {
                   setCancelContracts(true);
                  }}
                  style={{ marginRight: 5 }}
                  danger
                >
                  Hủy Hợp Đồng
                </Button>
              ) : (
                ""
              )}
              {contractInfo && job && client && freelancer ? (
                <Button
                  onClick={() => {
                    //xuathd();
                    generateDocument();
                  }}
                >
                  Xuất Hợp đồng
                </Button>
              ) : (
                ""
              )}
            </div>
            {accountType == "freelancer" && contractInfo.status == 0 && (
              <div style={{ display: "flex", justifyContent: "center" }}>
                <div style={{ width: 600 }}>
                  {checked ? (
                    <SignaturePadSimple
                      setImg={setImgSignature}
                    ></SignaturePadSimple>
                  ) : (
                    <></>
                  )}
                </div>
              </div>
            )}

            {contractInfo &&
            accountType == "freelancer" &&
            contractInfo.status <= 0 ? (
              <div className="text-center mt-6">
                {verify && contractInfo.status == 0 ? (
                  <Button
                    disabled={loading}
                    className="bg-button-primary hover:bg-button-primary/80 px-6 border-2 border-solid border-transparent rounded-[10rem] transition-all inline-flex justify-center items-center max-h-10 leading-[calc_2.5rem_-_1px] text-base font-medium disabled:bg-button-disabled disabled:text-[#9aaa97] disabled:!cursor-not-allowed disabled:pointer-events-auto"
                    onClick={() => {
                      if (address) onSubmit();
                      else connect();
                    }}
                  >
                    {loading && (
                      <ReloadIcon className="mr-2 h-4 w-4 animate-spin inline-flex" />
                    )}
                    {address ? "Ký hợp đồng" : "Kết nối ví MetaMask"}
                  </Button>
                ) : (
                  <Button
                    disabled={true}
                    className="bg-button-primary hover:bg-button-primary/80 px-6 border-2 border-solid border-transparent rounded-[10rem] transition-all inline-flex justify-center items-center max-h-10 leading-[calc_2.5rem_-_1px] text-base font-medium disabled:bg-button-disabled disabled:text-[#9aaa97] disabled:!cursor-not-allowed disabled:pointer-events-auto"
                    onClick={() => {}}
                  >
                    {loading && (
                      <ReloadIcon className="mr-2 h-4 w-4 animate-spin inline-flex" />
                    )}
                    {address ? "Ký hợp đồng" : "Kết nối ví MetaMask"}
                  </Button>
                )}
              </div>
            ) : (
              ""
            )}
          </div>
        </div>
      </div>

      <Modal
        className="text-center"
        title="Nhập mã OTP, mã OTP đã được gởi về mail của bạn"
        open={open}
        onCancel={hideModal}
        footer={[]}
      >
        <InputOtp setVerify={setVerify}></InputOtp>
      </Modal>

      <Modal
        className="text-center"
        title="Nhập vào lí do bạn không muốn kí hợp đồng"
        open={cancelContracts}
        onCancel={()=>{setCancelContracts(false)}}
        footer={[]}
      >
        <div>
          <Textarea style={{height:300}} onChange={(e:any)=>{setReasonCancel(e.target.value)}}></Textarea>
        </div>
        <div style={{ display: "flex", marginTop:20 , justifyContent: "center" }}>
                <Button
                  onClick={async() => {
              if (address) {
                      try {
                        setLoading(true);
                        await contract?.call('FreelancerNoSign', [contractInfo.contract_id, reasonCancel])
                        setLoading(false);
                        openNotificationWithIcon('success', 'Thành công', 'Bạn đã hủy thành công hợp đồng')
                        router.push('/');
                      } catch (error) {
                        openNotificationWithIcon('error', 'Thất bại', 'Thao tác thất bại vui lòng thử lại.')
                      }
              } else {
                connect();
                    }
                  }}
                  style={{ marginRight: 5 }}
                  danger
                >
                  {address ?"Hủy Hợp Đồng":"kết nối ví"}
          </Button>
          </div>
      </Modal>
    </div>
  );
};

export default ContractDetail;
