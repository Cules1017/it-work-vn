import { Textarea } from '@/app/components/ui/textarea';
import { NotificationContext } from '@/app/providers/NotificationProvider';
import { ReportList, ReportStatus } from '@/app/types/reports.type';
import { Button, Modal, Select, Upload } from 'antd';
import { UploadCloudIcon } from 'lucide-react';
import { ChangeEvent, useContext, useEffect, useState } from 'react';
import { IoMdCloudUpload } from 'react-icons/io';

interface ReportFormProps {
  isOpen: boolean;
  onCancel: () => void;
  onReport: (content: string, file: File | undefined) => void;
}

function ReportForm({ isOpen, onCancel, onReport }: ReportFormProps) {
  const [isModalConfirmOpen, setIsModalConfirmOpen] = useState(false);
  const [isModalCancelOpen, setIsModalCancelOpen] = useState(false);

  // Resolve
  const [resultText, setResultText] = useState('');
  const [selectedStatus, setSelectedStatus] = useState<ReportStatus>(0);

  // Upload file
  const [file, setFile] = useState<File | undefined>(undefined);
  const [fileName, setFileName] = useState<string>('');
  const handleFileChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const selectedFile = event.target.files ? event.target.files[0] : null;
    if (selectedFile) {
      setFile(selectedFile);
      setFileName(selectedFile.name);
    } else {
      setFile(undefined);
      setFileName('');
    }
  };

  return (
    <div>
      {isOpen && (
        <div>
          <Modal
            title=''
            open={isOpen}
            onOk={() => {}}
            onCancel={() => {
              onCancel();
            }}
            width={600}
            closable={false}
            footer={(_, { OkBtn, CancelBtn }) => (
              <div>
                <h2 className='text-center mb-3 text-[23px] font-bold'>
                  Báo cáo
                </h2>
                <div className='h-[400px] max-h-[400px] flex flex-col'>
                  <textarea
                    placeholder='Nội dung báo cáo'
                    className='flex-1 max-h-[300px] border outline-none p-4 rounded-sm resize-none'
                    value={resultText}
                    onChange={(e: ChangeEvent<HTMLTextAreaElement>) => {
                      setResultText(e.target.value);
                    }}
                  />
                  <div className='flex items-center p-2 pl-0'>
                    <input
                      type='file'
                      id='upload'
                      hidden
                      onChange={handleFileChange}
                    />
                    <label
                      htmlFor='upload'
                      className='flex items-center px-2 py-1 m-2 ml-0 rounded-md cursor-pointer border'
                    >
                      Tải tài liệu <IoMdCloudUpload className='ml-2' />
                    </label>
                    {fileName && <p>File: {fileName}</p>}
                  </div>

                  <div className='flex'>
                    <Button
                      disabled={resultText.length == 0}
                      className='mr-3 flex-1'
                      onClick={() => {
                        setIsModalConfirmOpen(true);
                      }}
                    >
                      Hoàn tất
                    </Button>
                    <Button
                      className='flex-1'
                      onClick={() => {
                        setIsModalCancelOpen(true);
                      }}
                    >
                      Hủy
                    </Button>
                  </div>
                </div>

                <Modal
                  title='Cảnh báo'
                  open={isModalConfirmOpen}
                  onOk={() => {}}
                  onCancel={() => {
                    setIsModalConfirmOpen(false);
                  }}
                  closable={false}
                  footer={(_, { OkBtn, CancelBtn }) => (
                    <>
                      <CancelBtn />
                      <Button
                        className='text-[#4096ff] border-[#4096ff] hover:text-[#fff] hover:bg-[#4096ff]'
                        onClick={async () => {
                          setIsModalConfirmOpen(false);
                          await onReport(resultText, file);
                          onCancel();
                        }}
                      >
                        Oke
                      </Button>
                    </>
                  )}
                >
                  <p>Bạn có muốn hoàn tất xử lý ?</p>
                </Modal>

                <Modal
                  title='Cảnh báo'
                  open={isModalCancelOpen}
                  onOk={() => {}}
                  onCancel={() => {
                    setIsModalCancelOpen(false);
                  }}
                  closable={false}
                  footer={(_, { OkBtn, CancelBtn }) => (
                    <>
                      <CancelBtn />
                      <Button
                        className='text-[#4096ff] border-[#4096ff] hover:text-[#fff] hover:bg-[#4096ff]'
                        onClick={() => {
                          setIsModalCancelOpen(false);
                          setTimeout(() => {
                            onCancel();
                          }, 100);
                        }}
                      >
                        Oke
                      </Button>
                    </>
                  )}
                >
                  <p>Bạn có muốn hủy ?</p>
                </Modal>
              </div>
            )}
          ></Modal>
        </div>
      )}
    </div>
  );
}

export default ReportForm;
