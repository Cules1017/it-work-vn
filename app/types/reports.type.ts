export enum ReportType {
  Freelancer = 1,
  Client = 2,
  Job = 3,
  Other = 4,
}

export enum ReportStatus {
  Reject = 0,
  Approve = 1,
}

export type Reporter = {
  id: number;
  username: string;
  first_name: string;
  last_name: string;
  email: string;
  avatar_url: string;
};

export type Post = {
  title: string;
};

export type ReportList = {
  id: 1;
  client_id: number;
  client: Reporter;
  freelancer_id: number;
  freelancer: Reporter;
  post_id: number;
  post: Post;
  type_id: ReportType;
  content: string;
  content_file: string;
  results: string;
  status?: ReportStatus;
  created_at: Date;
  updated_at: Date;
};
