import relativeTime from "dayjs/plugin/relativeTime";
import dayjs from "dayjs";
import Link from "next/link";
import { BadgeCheck } from "lucide-react";
import { DetailJobPost } from "@/app/types/freelancer.type";
import { Rate } from "antd";

dayjs.extend(relativeTime).locale('vi');

interface IPostItem {
  post: DetailJobPost;
}
const PostItem: React.FC<IPostItem> = ({ post }) => {
  return (
    <div className='w-full bg-white border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700 my-3'>
      <Link
        href={`/freelancer/job/${post.id}`}
        className='block p-6 group border-b border-solid border-[#d5e0d5] hover:bg-[#f2f7f2] last-of-type:border-transparent'
      >
        <div className='flex justify-around items-center'>
          <div className='w-[60%]'>
            <small className='text-sm mb-3 font-normal text-[#5e6d55]'>
              {dayjs(post.updated_at).fromNow()}
            </small>
            <h4 className='mb-1 text-2xl leading-7 tracking-[0.03px] font-medium text-black group-hover:text-[#14a800] group-hover:underline'>
              {post.title}
            </h4>
            {/* <p className="text-sm text-[#5e6d55] mb-2">{post.desc}</p> */}
            <div
              style={{ padding: 20 }}
              dangerouslySetInnerHTML={{ __html: post?.desc || '' }}
            />
            
            
            <div className='mb-6'>
              <h3 className='text-lg font-medium mb-2'>Kỹ năng</h3>
              <div className='flex items-center gap-x-3'>
                {post?.skills?.length === 0
                  ? 'Không có skill nào được chọn'
                  : post?.skills?.map((s) => (
                      <div
                        key={`selected-skill-${s.skill_id}`}
                        className='cursor-pointer flex items-center gap-x-1 border-2
                     border-solid border-transparent px-3 rounded-2xl h-8 text-sm font-medium 
                     leading-[31px] bg-[#108a00] hover:bg-[#14a800] text-white'
                        onClick={() => {}}
                      >
                        {s.skill_name}
                      </div>
                    ))}
              </div>
            </div>
            
            <div className='text-sm text-[#5e6d55] mb-3 flex items-center'>
              <p>
                <BadgeCheck
                  className='w-5 h-5 inline-flex -mt-[3px] text-white'
                  fill='#5e6d55'
                />{' '}
                Đã xác minh
              </p>
              <span className='ml-4'>{`${post.bids}$`}</span>
             
            </div>
            <div className='mb-6'>Người tạo: <Link href={`/show-detail-info/client/${post.client_id}`}>{post.clientName}</Link>  <span  style={{marginLeft:10}}><Rate disabled defaultValue={post.rate}></Rate></span></div>
          </div>
          <div className=' flex justify-center'>
            <div className='w-[250px] h-[250px] flex justify-center'>
              <img
                src={post?.thumbnail}
                alt={post.title}
                className='w-[100%] h-[100%] object-cover rounded-lg'
              />
            </div>
          </div>
        </div>
      </Link>
    </div>
  );
};

export default PostItem;
