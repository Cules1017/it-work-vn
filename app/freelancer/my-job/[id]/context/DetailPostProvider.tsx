'use client';
import { NotificationContext } from "@/app/providers/NotificationProvider";
import { clientServices } from "@/app/services/client.services";
import { DetailClientPost } from "@/app/types/client.types";
import { Spin } from "antd";
import { createContext, useCallback, useContext, useState } from "react";
interface IDetailPostContext {
  post: DetailClientPost | null;
  loading: boolean;
  handleGetPostDetail?: (postId: string) => void;
}
export const DetailPostContext = createContext<IDetailPostContext>({
  post: null,
  loading: false,
});

export const DetailPostProvider = ({
  children,
}: {
  children: React.ReactNode;
}) => {
  const { openNotificationWithIcon } = useContext(NotificationContext);
  const [loading, setLoading] = useState(false);
  const [post, setPost] = useState<DetailClientPost | null>(null);

  const handleGetPostDetail = useCallback(async (postId: string) => {
    try {
      setLoading(true);
      const res = await clientServices.getPost(postId);
      if (res.data) {
        setPost(res.data);
        // openNotificationWithIcon(
        //     "success",
        //     "Thành công",
        //     "Lấy thông tin thất bại."
        //   );
      }
    } catch (error) {
      setLoading(false);
      openNotificationWithIcon("error", "Thất bại", "Lấy thông tin thất bại.");
    } finally {
      setLoading(false);
    }
  }, []);

  return (
    // <DetailPostContext.Provider value={{}}>
    //     {children}
    // </DetailPostContext.Provider>
    <DetailPostContext.Provider
      value={{
        post,
        loading,
        handleGetPostDetail,
      }}
    >
      {loading && <Spin fullscreen></Spin>}
      {children}
    </DetailPostContext.Provider>
  );
};
