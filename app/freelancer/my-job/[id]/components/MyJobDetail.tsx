"use client";

import { Button } from "antd";
import { Skeleton } from "@/app/components/ui/skeleton";
import { appConfig } from "@/app/configs/app.config";
import { NotificationContext } from "@/app/providers/NotificationProvider";
import { freelancerServices } from "@/app/services/freelancer.services";
import { smartContractService } from "@/app/services/thirbWeb.services";
import { DetailClientPost } from "@/app/types/client.types";
import { AppliedJob } from "@/app/types/freelancer.type";
import { useStateContext } from "@/context";
import { useContract } from "@thirdweb-dev/react";
import { Modal } from "antd";
import TextArea from "antd/es/input/TextArea";
import { format } from "date-fns";
import { FileIcon, SquarePen } from "lucide-react";
import Image from "next/image";
import Link from "next/link";
import { useContext, useEffect, useState } from "react";
import { VscFile } from "react-icons/vsc";
import { sendNotification } from "@/app/utils/helpers";

interface IMyJobDetail {
  jobId: string;
}

const MyJobDetail: React.FC<IMyJobDetail> = ({ jobId }) => {
  const [loading, setLoading] = useState(false);
  const [post, setPost] = useState<DetailClientPost | null>(null);
  const [jobs, setJobs] = useState<AppliedJob[]>([]);
  const { address, connect } = useStateContext();
  // const { contract } = useContract(appConfig.contractId);
  // const [policy, setPolicy] = useState<any>();
  // const [isOpenedModal, setIsOpenedModal] = useState(false);
  // const [loadingUpdatePolicy, setLoadingUpdatePolicy] =
  //   useState<boolean>(false);
  let { openNotificationWithIcon } = useContext(NotificationContext);
  // const [contractInfo, setContractInfo] = useState({
  //   contract_id: -1,
  //   title: "",
  //   description: "",
  //   signature_freelancer: "",
  //   signature_client: "",
  //   bids: "0",
  //   status: 0,
  //   address_client: 0,
  //   address_freelancer: 0,
  //   freelancer_id: 0,
  //   client_id: 0,
  //   listPolicy: [],
  // });

  // const getContractByJobId = async (id: any) => {
  //   const data = await smartContractService.getDetailContractByJobId(
  //     parseInt(id),
  //     contract
  //   );
  //   console.log("data------->", data);
  //   if (data) {
  //     setContractInfo(data);
  //   }
  // };

  useEffect(() => {
    const fetchFreelancerJobs = async () => {
      const res = await freelancerServices.getAppliedJobs({});
      setJobs(res?.data?.data);
    };
    fetchFreelancerJobs();
  }, []);

  useEffect(() => {
    const fetchPostData = async (jobId: string) => {
      try {
        setLoading(true);
        const res = await freelancerServices.getPost(jobId);
        if (res.data) {
          setPost(res.data);
        }
        // const data = await getContractByJobId(jobId);
      } catch (error) {
        console.log("error", error);
      } finally {
        setLoading(false);
      }
    };
    if (jobId) {
      fetchPostData(jobId);
    }
  }, [jobId]);
  // useEffect(() => {
  //   setPolicy(contractInfo?.listPolicy[contractInfo?.listPolicy.length - 1]);
  // }, [contractInfo]);
  // console.log("policy---->", policy);

  //accept policy
  // const acceptPolicySmartContract = async () => {
  //   if (address) {
  //     setLoadingUpdatePolicy(true);
  //     try {
  //       const data = await smartContractService.acceptPolicyContract(
  //         contractInfo.contract_id,
  //         contract
  //       );
  //       if (data) {
  //         console.log("updatePolicySmartContract", data);
  //       }
        
  //       sendNotification({
  //         title: `Những thay đổi trong điều khoản hợp đồng đã được freelancer chấp nhận 😍`,
  //         message: 'Những thay đổi trong điều khoản hợp đồng đã được freelancer chấp nhận',
  //         linkable: `/info-contract/${contractInfo?.contract_id}`,
  //         smail: 1,
  //         imagefile: null,
  //         user_type: "client",
  //         user_id: contractInfo?.client_id,
  //       });
  //       openNotificationWithIcon(
  //         "success",
  //         "Thành công",
  //         "Hợp đồng của bạn đã được cập nhật lại thành công."
  //       );
  //     } catch (error) {
  //       openNotificationWithIcon(
  //         "error",
  //         "Thất bại",
  //         "Hợp đồng của bạn đã được cập nhật lại thất bại."
  //       );
  //     } finally {
  //       setLoadingUpdatePolicy(false);
  //       setIsOpenedModal(false);
  //     }
  //   } else {
  //     // setLoadingUpdatePolicy(false);
  //     connect();
  //   }
  // };

  return (
    <>
      <section>
        <div>
          <h2 className="text-4xl font-semibold -tracking-[1px]">
            Chi tiết công việc
          </h2>
        </div>
        <div className="my-8 border border-solid border-[#d5e0d5] rounded-[16px]">
          <article>
            <header className="p-8 border-b border-solid border-[#d5e0d5] flex items-center justify-between">
              {loading ? (
                <Skeleton className="w-full h-7" />
              ) : (
                <h3 className="text-2xl font-medium">{post?.title || ""}</h3>
              )}
            </header>

            <div className="p-8 pb-0">
              <h3 className="text-lg font-medium mb-2">Mô tả ngắn</h3>
              <div className="flex items-center justify-between">
                {loading ? (
                  <Skeleton className="w-full h-4" />
                ) : (
                  // <p>{post?.desc}</p>
                  <div dangerouslySetInnerHTML={{ __html: post?.desc || "" }} />
                )}
              </div>
            </div>

            <div className="p-8 pb-0">
              <h3 className="text-lg font-medium mb-2">Nội dung</h3>
              <div className="flex items-center justify-between">
                {loading ? (
                  <Skeleton className="w-full h-20" />
                ) : (
                  <>
                    {/* <p>{post?.content}</p> */}
                    <div
                      // style={{ padding: 20 }}
                      dangerouslySetInnerHTML={{ __html: post?.content || "" }}
                    />
                  </>
                )}
              </div>
            </div>

            <div className="p-8">
              <div className="mb-6">
                <h3 className="text-lg font-medium mb-2">Kỹ năng</h3>
                <div className="flex items-center gap-x-3">
                  {loading ? (
                    <>
                      <Skeleton className="w-20 h-7" />
                      <Skeleton className="w-20 h-7" />
                      <Skeleton className="w-20 h-7" />
                    </>
                  ) : (
                    post?.skills.map((s) => (
                      <div
                        key={`selected-skill-${s.skill_id}`}
                        className="cursor-pointer flex items-center gap-x-1 border-2 border-solid border-transparent px-3 rounded-2xl h-8 text-sm font-medium leading-[31px] bg-[#108a00] hover:bg-[#14a800] text-white"
                        onClick={() => {}}
                      >
                        {s.skill_name}
                      </div>
                    ))
                  )}
                </div>
              </div>
              <div className="mb-6 flex items-center">
                <h3 className="text-lg font-medium ">Thời hạn công việc</h3>
                <div className="flex items-center gap-x-3 pl-3">
                  {loading ? (
                    <Skeleton className="h-[20px] w-[250px]" />
                  ) : (
                    <p>
                      {post?.deadline &&
                        format(post.deadline || "", "dd-MM-yyyy")}
                    </p>
                  )}
                </div>
              </div>
              <div className="mb-6 flex items-center">
                <h3 className="text-lg font-medium min-w-[130px]">
                  File đính kèm
                </h3>
                <div className="flex items-center gap-x-3 pl-3">
                  {!loading && post?.content_file && (
                    <Link href={post?.content_file} target="_blank">
                      <div className="upload-file">
                        <div className="flex px-3 py-3">
                          <div className="upload-file-thumbnail !p-0 w-8 h-8">
                            {
                              <FileIcon>
                                <VscFile />
                              </FileIcon>
                            }
                          </div>
                        </div>
                      </div>
                    </Link>
                  )}
                </div>
              </div>
              <div className="mb-6 flex items-start">
                <h3 className="text-lg font-medium mb-2 min-w-[130px]">
                  Hình ảnh
                </h3>
                <div className="flex items-center gap-x-3 pl-3">
                  {!loading && post?.thumbnail && (
                    <Link href={post?.thumbnail} target="_blank">
                      <div className="w-[120px] h-[120px] relative">
                        <Image src={post?.thumbnail} alt="" fill />
                      </div>
                    </Link>
                  )}
                </div>
              </div>
            </div>

            <footer className="p-8 border-t border-solid border-[#d5e0d5] flex items-center justify-end">
              {post?.status?.toString() === "3" && (
                <div className="flex flex-row justify-end">
                  <Button
                    size="large"
                    className="cursor-pointer flex items-center border-solid border-transparent text-sm font-medium  bg-[#108a00] hover:bg-[#14a800] text-white mr-4 "
                  >
                    <Link
                      href={{
                        pathname: `/freelancer/my-job/${jobId}/task`,
                      }}
                    >
                      Quản lý chi tiết công việc
                    </Link>
                  </Button>
                  {/* {policy?.accepted == 0 && (
                    <Button
                      size="large"
                      className="cursor-pointer flex items-center border-solid border-transparent text-sm font-medium 
                  bg-[#108a00] hover:bg-[#14a800] text-white mr-4"
                      onClick={() => setIsOpenedModal(true)}
                    >
                      Xem điều khoản thay đổi
                    </Button>
                  )} */}

                  <Button
                    size="large"
                    className="cursor-pointer flex items-center border-solid border-transparent text-sm font-medium 
               bg-[red] hover:bg-red-300 text-white mr-4"
                  >
                    <Link
                      href={{
                        pathname: `/freelancer/my-job/${post?.id}/cancel-contract`,
                      }}
                    >
                      Hủy hợp đồng
                    </Link>
                  </Button>
                </div>
              )}
              {post?.status?.toString() === "2" && (
                <>
                  <Button
                    size="large"
                    className="cursor-pointer flex items-center border-solid border-transparent text-sm font-medium  bg-[#108a00] hover:bg-[#14a800] text-white mr-4 "
                  >
                    <Link
                      href={{
                        pathname: `/freelancer/job/${jobId}/apply`,
                      }}
                    >
                      Ứng tuyển ngay
                    </Link>
                  </Button>
                  <Button
                    size="large"
                    className="cursor-pointer flex items-center border-2 border-solid-[#14a800] border-transparent text-sm font-medium  bg-[#108a00] hover:bg-[#14a800] text-white mr-4"
                  >
                    <Link
                      href={`/freelancer/apply/${jobId}`}
                      className="flex gap-x-3"
                    >
                      <SquarePen color="#fff" className="w-5 h-5" />
                      <span>Lưu công việc</span>
                    </Link>
                  </Button>
                </>
              )}
            </footer>
          </article>
        </div>
      </section>
      {/* <Modal
        open={isOpenedModal}
        title="Nội dung chỉnh sửa điều khoản hợp đồng"
        width={800}
        className="min-h-[400px]"
        onCancel={() => {
          setIsOpenedModal(false);
        }}
        footer={[
          <Button
            loading={loadingUpdatePolicy}
            className="bg-green-500 hover:bg-green-700 text-white font-bold  mr-4"
            key="ok"
            onClick={() => acceptPolicySmartContract()}
          >
            {address ? "Chấp nhận" : "Connect wallet"}
          </Button>,
          <Button
            key="cancel"
            className="bg-red-500 hover:bg-red-700 text-white font-bold"
            onClick={() => {
              setIsOpenedModal(false);
            }}
          >
            Hủy bỏ
          </Button>,
        ]}
      >
        <TextArea
          defaultValue={policy?.description}
          placeholder="Nhập nội dung"
          autoSize={{ minRows: 5, maxRows: 10 }}
          disabled
        />
      </Modal> */}
    </>
  );
};

export default MyJobDetail;
