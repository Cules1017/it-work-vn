"use client";
import React from "react";
import { DetailPostProvider } from "../context/DetailPostProvider";
import CancelContractDetail from "./components/CancelContractDetail";
// import CreateContractDetail from "./components/CreateContractDetail";

interface ICreateContract {
  params: {
    id: string;
  };
}
const CreateContract: React.FC<ICreateContract> = ({ params }) => {
  return (
    <main className="container w-[80%]">
      <DetailPostProvider>
        <CancelContractDetail postId={params.id} />
      </DetailPostProvider>
    </main>
  );
};
export default CreateContract;
