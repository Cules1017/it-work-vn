import React, { useContext, useEffect } from "react";
import { Skeleton } from "@/app/components/ui/skeleton";
import CancelFormContract from "./FormCancel";
import { DetailPostContext } from "../../context/DetailPostProvider";
interface ICancelContract {
  postId: string;
}
const CancelContractDetail = ({ postId }: ICancelContract) => {
  const { post, loading, handleGetPostDetail } = useContext(DetailPostContext);
  useEffect(() => {
    if (postId) {
      handleGetPostDetail?.(postId);
    }
  }, [postId]);
  return loading ? (
    <>
      <Skeleton className="w-20 h-7" />
      <Skeleton className="w-20 h-7" />
      <Skeleton className="w-20 h-7" />
    </>
  ) : (
    <section>

      {post && post?.nominee ? (
        <CancelFormContract post={post} />
      ) : (
        <></>
      )}
    </section>
  );
};

export default CancelContractDetail;
