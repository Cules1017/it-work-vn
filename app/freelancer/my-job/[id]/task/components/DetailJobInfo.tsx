"use client";

import { freelancerServices } from "@/app/services/freelancer.services";
import { AppliedJob } from "@/app/types/freelancer.type";
import { DetailClientPost } from "@/app/types/client.types";
import React, {
  use,
  useCallback,
  useContext,
  useEffect,
  useState,
} from "react";
import TaskBoardProvider from "./TaskBoard/TaskBoardContext";
import TaskBoard from "./TaskBoard";
import { Button, notification } from "antd";
import { useContract } from "@thirdweb-dev/react";
import { appConfig } from "@/app/configs/app.config";
import { NotificationContext } from "@/app/providers/NotificationProvider";
import { useRouter } from "next/navigation";
import { ReloadIcon } from "@radix-ui/react-icons";
import { useStateContext } from "@/context";
import { smartContractService } from "@/app/services/thirbWeb.services";
import { commonServices } from "@/app/services/common.services";
interface IDetailJobInfor {
  id: string;
}
const DetailJobInfor: React.FC<IDetailJobInfor> = ({ id }) => {
  const [loading, setLoading] = useState(false);
  const [post, setPost] = useState<DetailClientPost | null>(null);
  const [jobs, setJobs] = useState<AppliedJob[]>([]);
  const { openNotificationWithIcon } = useContext(NotificationContext);
  const [loadingRequestComplete, setLoadingRequestComplete] = useState(false);
  const router = useRouter();
  const { address, connect } = useStateContext();
  const { contract } = useContract(appConfig.contractId);
  const [infoContract, setInfoContract] = useState<any>(null);

  useEffect(() => {
    const fetchFreelancerJobs = async () => {
      const res = await freelancerServices.getAppliedJobs({});
      setJobs(res?.data?.data);
    };
    fetchFreelancerJobs();
  }, []);

  const sendNotification = async (data: any) => {
    try {
      // setIsGettingPosts(true);
      const res = await commonServices.sendNotication(data);
      if (res.status === 200) {
        console.log("send notification success", res);
      }
    } catch (error) {
      console.log("error", error);
    } finally {
      // setIsGettingPosts(false);
    }
  };

  useEffect(() => {
    const fetchPostData = async (jobId: string) => {
      try {
        setLoading(true);
        const res = await freelancerServices.getPost(jobId);
        if (res.data) {
          setPost(res.data);
        }
      } catch (error) {
      } finally {
        setLoading(false);
      }
    };
    if (id) {
      fetchPostData(id);
    }
  }, [id]);

  useEffect(() => {
    const getContractDetail = async () => {
      if (contract) {
        const data = await smartContractService.getDetailContractByJobId(
          parseInt(id),
          contract
        );
        setInfoContract(data);
        console.log("DATA 11", data);
      }
    };
    getContractDetail();
  }, [contract]);

  // Xử lý hoàn thành hợp đồng
  const handleCompletedJob = useCallback(() => {
    const checkData = async () => {
      try {
        setLoadingRequestComplete(true);
        const data = await contract?.call("reportCompletion", [
          infoContract?.contract_id,
        ]);
        setLoadingRequestComplete(false);
        if (data != undefined) {
          openNotificationWithIcon(
            "success",
            "Thành công",
            "Đã báo cáo hoàn thành công việc thành công."
          );
          sendNotification({
            title: `Thông báo`,
            message:  `Yêu cầu xác nhận công việc hoàn thành từ ${post?.nominee?.username} 😍`,
            linkable: `client/task-management/${id}`,
            smail: 1,
            imagefile: null,
            user_type: "client",
            user_id: post?.client_id,
          });
          router.push(`/freelancer/my-job/${id}/reviewing-job`);
        } else {
          openNotificationWithIcon(
            "error",
            "Thất bại",
            "Gửi yêu cầu review job"
          );
        }
      } catch (error) {
        openNotificationWithIcon("error", "Thất bại", "Gửi yêu cầu review job");
        // setLoading(false);
      } finally {
        setLoadingRequestComplete(false);
      }
    };
    if (address) {
      checkData();
    } else {
      connect();
    }
  }, [contract, id]);

  return (
    <section>
      <div className="flex justify-between">
        <div>
          <p className="text-3xl font-semibold -tracking-[1px]">
            Quản lý task - {post?.title}
          </p>
        </div>
        <div className="">
          {infoContract?.status === 1 ? (
            <Button
              type="primary"
              className="!bg-blue-500"
              size="large"
              onClick={handleCompletedJob}
            >
              {loadingRequestComplete && (
                <ReloadIcon className="mr-2 h-4 w-4 animate-spin inline-flex" />
              )}
              {address
                ? "Báo cáo hoàn thành"
                : "Kết nối ví MetaMask để báo cáo hoàn thành"}
            </Button>
          ) : infoContract?.status === 2 ? (
            <Button
              type="primary"
              className="!bg-green-500"
              size="large"
              //  onClick={handleCompletedJob}
            >
              Đang chờ xác nhận
            </Button>
          ): infoContract?.status === 3 ? (
            <Button
              type="primary"
              className="!bg-green-500"
              size="large"
              //  onClick={handleCompletedJob}
            >
              Đã hoàn thành
            </Button>
          ) : null
          }
        </div>
      </div>
      <div className="my-8">
        <TaskBoardProvider>
          <TaskBoard jobId={id} />
        </TaskBoardProvider>
      </div>
    </section>
  );
};

export default DetailJobInfor;
