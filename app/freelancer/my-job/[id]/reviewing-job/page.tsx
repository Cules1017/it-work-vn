'use client';
import React from "react";
import { DetailPostProvider } from "../context/DetailPostProvider";
import DetailPage from "./components/DetailPage";
interface IReviewing {
  params: {
    id: string;
  };
}
const Reviewing: React.FC<IReviewing> = ({ params }) => {
  return (
    <DetailPostProvider>
      <DetailPage postId={params.id} />
    </DetailPostProvider>
  );
};

export default Reviewing;
