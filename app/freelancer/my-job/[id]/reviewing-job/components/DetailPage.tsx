"use client";
import { Button, Result, Skeleton } from "antd";
import React, { useContext, useEffect } from "react";
import { DetailPostContext } from "../../context/DetailPostProvider";
import { useRouter } from "next/navigation";
interface IDetailPage {
  postId: string;
}
const DetailPage = ({ postId }: IDetailPage) => {
  const { post, loading, handleGetPostDetail } = useContext(DetailPostContext);
  // const searchParams = useSearchParams();
  const router = useRouter();
  useEffect(() => {
    if (postId) {
      handleGetPostDetail?.(postId);
    }
  }, [postId]);

  return loading ? (
    <>
      <Skeleton className="w-20 h-7" />
      <Skeleton className="w-20 h-7" />
      <Skeleton className="w-20 h-7" />
    </>
  ) : (
    post && (
      <section>
        <Result
          status="success"
          title={`Công việc ${post.title} của bạn đang client được review!`}
          // ${`Công việc này của bạn đang client được review!`}
          subTitle={
            <div
              // style={{ padding: 20 }}
              dangerouslySetInnerHTML={{ __html: post?.desc || "" }}
            />
          }
          extra={[
            <Button
              type="primary"
              className="!bg-blue-500"
              size="large"
              // key="console"
              onClick={()=>{
                router.push(`/freelancer/my-job/${post.id}`);
              }}
            >
              Đi về trang chi tiết công việc
            </Button>,
          ]}
        />
      </section>
    )
  );
};

export default DetailPage;
