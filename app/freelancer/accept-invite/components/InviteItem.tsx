"use client";
import { Button } from "@/app/components/ui/button";
import { Skeleton } from "@/app/components/ui/skeleton";
import Link from "next/link";
import React, { useContext, useState } from "react";
import { Invite } from "@/app/types/freelancer.type";
import { freelancerServices } from "@/app/services/freelancer.services";
import constants from "@/app/utils/constants";
import { commonServices } from "@/app/services/common.services";
import { AuthContext } from "@/app/providers/AuthProvider";
import { notification } from "antd";
import { useRouter } from "next/navigation";
import { ReloadIcon } from "@radix-ui/react-icons";
import { BadgeCheck } from "lucide-react";
import { NotificationContext } from "@/app/providers/NotificationProvider";
// import InviteFreelancerDialog from "./dialog/InviteFreelancerDialog";

interface IInviteItem {
  invite: Invite;
}
type NotificationType = "success" | "info" | "warning" | "error";
const InviteItem = ({ invite }: IInviteItem) => {
  const { user } = useContext(AuthContext);
  const [loading, setLoading] = useState(false);
  const [loadingReject, setLoadingReject] = useState(false);
  const router = useRouter();
  const { openNotificationWithIcon } = useContext(NotificationContext);
  const sendNotification = async (data: any) => {
    try {
      // setIsGettingPosts(true);
      const res = await commonServices.sendNotication(data);
      if (res.status === 200) {
        console.log("send notification success", res);
      }
    } catch (error) {
      console.log("error", error);
    } finally {
      // setIsGettingPosts(false);
    }
  };

  const handleAcceptInvite = async (id: number) => {
    try {
      setLoading(true);
      const res = await freelancerServices.handleResponseInvite(
        id,
        constants.ACCEPT_INVITE
      );
      if (res.status === 200) {
        sendNotification({
          title: "Thông báo",
          message: `${user?.username} đã đồng ý lời mời làm việc của bạn 😍`,
          linkable: `${invite.job_id}`,
          smail: 1,
          imagefile: null,
          user_type: "client",
          user_id: invite.client_id,
        });
        openNotificationWithIcon("success", "Đồng ý lời mời thành công");
        router.push(`/freelancer/job/${invite.job_id}`);
      } else {
        openNotificationWithIcon("error", res.message || "Đã có lỗi xảy ra");
      }
      // if(res)
    } catch (error) {
      openNotificationWithIcon("error", "Đã có lỗi xảy ra");
    } finally {
      setLoading(false);
    }
  };
  const handleRejectInvite = async (id: number) => {
    try {
      setLoadingReject(true);
      const res = await freelancerServices.handleResponseInvite(
        id,
        constants.REJECT_INVITE
      );
      setLoadingReject(false);
      if (res.status === 200) {
        sendNotification({
          title: "Thông báo",
          message: `${user?.username} đã từ chối lời mời làm việc của bạn 😂`,
          linkable: `client/post/${invite.job_id}`,
          smail: 1,
          imagefile: null,
          user_type: "client",
          user_id: invite.client_id,
        });
        openNotificationWithIcon("success", "Từ chối lời mời thành công");
      } else {
        openNotificationWithIcon("error", res.message || "Đã có lỗi xảy ra");
      }
      // if(res)
    } catch (error) {
      openNotificationWithIcon("error", "Đã có lỗi xảy ra");
    } finally {
      setLoadingReject(false);
    }
  };

  return (
    <>
      <div className="mb-6 items-start">
        <div className="w-full bg-white border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700">
          <div className="p-6 bg-white border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700">
            <div className="flex items-center content-center">
              <div className="flex justify-around items-center w-[80%]">
                {/* Content job */}
                <div className="w-[60%]">
                  <p className="mb-3">
                    Lời mời làm việc từ:{" "}
                    <strong>{invite.client_info.username}</strong>
                  </p>
                  <p className="mb-3">
                    Tên công việc <strong>{invite.job_info.title}</strong>
                  </p>
                  <div className="text-sm text-[#5e6d55] mb-3 flex items-center">
                    <p className="mb-3">
                      Tiền công: <strong>{invite.job_info.bids}</strong>
                    </p>
                  </div>
                </div>
                {/* image */}
                <div className=" flex justify-center">
                  <div className="w-[200px] h-[200px] flex justify-center">
                    <img
                      src={invite.job_info?.thumbnail || "/images/no-image.png"}
                      alt={invite.job_info.title}
                      className="w-[100%] h-[100%] object-cover rounded-lg"
                    />
                  </div>
                </div>
              </div>
            </div>
            {/* info client */}
            <div className="m-2">
              <p className="">
                {" "}
                <strong>Nội dung lời mời</strong> {invite.title}
              </p>
            </div>
            {/* intro */}
            {Number(invite.status) === constants.PENDING_INVITE ? (
              <div className="flex items-center content-center">
                <Button
                  asChild
                  variant="default"
                  className="text-white bg-blue-500 hover:bg-blue-400 mr-4"
                >
                  <Link
                    // target="_blank"
                    href={`freelancer/job/${invite.job_id}`}
                  >
                    Chi tiết công việc
                  </Link>
                </Button>
                <Button
                  // asChild
                  variant="default"
                  className="text-white bg-green-500 hover:bg-green-400 cursor-pointer mr-4"
                  onClick={() => {
                    handleAcceptInvite(invite.id);
                  }}
                >
                  {loading && (
                    <ReloadIcon className="mr-2 h-4 w-4 animate-spin inline-flex" />
                  )}
                  <span>Chấp nhận lời mời làm việc</span>
                </Button>
                <Button
                  // asChild
                  variant="default"
                  className="text-white bg-red-400 hover:bg-red-200 cursor-pointer"
                  onClick={() => {
                    handleRejectInvite(invite.id);
                  }}
                >
                  {loadingReject && (
                    <ReloadIcon className="mr-2 h-4 w-4 animate-spin inline-flex" />
                  )}
                  Từ chối lời mời làm việc
                </Button>
              </div>
            ) : Number(invite.status) === constants.ACCEPT_INVITE ? (
              <div className="flex items-center content-center">
                <Button
                  variant="default"
                  className="text-white bg-blue-500 hover:bg-blue-400 mr-4"
                >
                  <Link
                    // target="_blank"
                    href={`freelancer/job/${invite.job_id}`}
                  >
                    Chi tiết công việc
                  </Link>
                </Button>
                <Button
                  // asChild
                  variant="default"
                  className="text-white bg-green-500 hover:bg-green-400 cursor-pointer mr-4"
                >
                  {/* {loading && (
                    <ReloadIcon className="mr-2 h-4 w-4 animate-spin inline-flex" />
                  )} */}
                  <span>Đã chấp nhận lời mời làm việc</span>
                </Button>
              </div>
            ) : (
              <div className="flex items-center content-center">
                <Button
                  variant="default"
                  className="text-white bg-blue-500 hover:bg-blue-400 mr-4"
                >
                  <Link
                    // target="_blank"
                    href={`freelancer/job/${invite.job_id}`}
                  >
                    Chi tiết công việc
                  </Link>
                </Button>
                <Button
                  // asChild
                  variant="default"
                  className="text-white bg-red-400 hover:bg-red-200 cursor-pointer"
                >
                  <span>Đã từ chối lời mời làm việc</span>
                </Button>
              </div>
            )}

            {/* {!loading && post?.nominee?.attachment_url && ( */}
          </div>
        </div>
      </div>

      {/* <InviteFreelancerDialog
        isOpen={showInviteModal}
        freelancer={freelancer}
        onClose={() => setShowInviteModal(false)}
      /> */}
    </>
  );
};

export default InviteItem;
