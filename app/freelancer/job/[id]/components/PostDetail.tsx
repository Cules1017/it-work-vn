"use client";

import { Button } from "@/app/components/ui/button";
import { Skeleton } from "@/app/components/ui/skeleton";
import { freelancerServices } from "@/app/services/freelancer.services";
import { DetailClientPost } from "@/app/types/client.types";
import { format } from "date-fns";
import { Bug, FileIcon, SquarePen } from "lucide-react";
import Image from "next/image";
import Link from "next/link";
import { useContext, useEffect, useState } from "react";
import { VscFile } from "react-icons/vsc";
import AppliedTable from "./AppliedTable";
import InviteTable from "./InviteTable";
import { IoMdMore } from "react-icons/io";
import { useRouter } from "next/navigation";
import ReportForm from "../../../../(protected)/components/ReportForm";
import { NotificationContext } from "@/app/providers/NotificationProvider";
import { commonServices } from "@/app/services/common.services";
import { AuthContext } from "@/app/providers/AuthProvider";
import Cookies from "js-cookie";
import { ReportType } from "@/app/types/reports.type";

interface IPostDetail {
  postId: string;
}

const PostDetail: React.FC<IPostDetail> = ({ postId }) => {
  const [loading, setLoading] = useState(false);
  const [post, setPost] = useState<DetailClientPost | null>(null);
  const { openNotificationWithIcon } = useContext(NotificationContext);
  const { user } = useContext(AuthContext);
  const user_type = Cookies.get("account_type");

  useEffect(() => {
    const fetchPostData = async (postId: string) => {
      try {
        setLoading(true);
        const res = await freelancerServices.getPost(postId);
        if (res.data) {
          setPost(res.data);
        }
      } catch (error) {
      } finally {
        setLoading(false);
      }
    };
    if (postId) {
      fetchPostData(postId);
    }
  }, [postId]);

  const [isOpenModel, setOpenModal] = useState(false);

  const handleReport = async (content: string, file: File | undefined) => {
    if (!loading && user) {
      try {
        const formData = new FormData();
        if (file) formData.append("content_file", file);
        if (user_type == "client")
          formData.append("client_id", user.id.toString());
        if (user_type == "freelancer")
          formData.append("freelancer_id", user.id.toString());
        formData.append("content", content);
        formData.append("type_id", ReportType.Job.toString());
        formData.append("post_id", postId);

        const res = await commonServices.report(formData);
        if (res.status == 200) {
          openNotificationWithIcon(
            "success",
            "Thành công",
            "Báo cáo thành công"
          );
        } else openNotificationWithIcon("error", "Lỗi", res.message);
      } catch (error) {
        openNotificationWithIcon("error", "Lỗi", error);
      }
    }
  };
  const isCheckHaveApplied = post?.applied?.find(
    (item) => item?.freelancer_id === user?.id.toString()
  );
  // const isCheckHaveApplied = () => {
  //   if (post?.applied) {
  //     if (user) {
  //       const check = post?.applied?.find(
  //         (item) => item?.freelancer_id === user.id.toString()
  //       );

  //       return check ? true : false;
  //     }
  //   }
  // };
  // console.log('aaaaaaaaaaaaaa', isCheckHaveApplied());

  return (
    <section className="px-20">
      <div className="flex">
        <h2 className="text-4xl font-semibold -tracking-[1px] flex-1">
          Chi tiết công việc
        </h2>
        <div className="p-2 group z-50 relative">
          <div className="  px-4 py-1 top-6 left-2 w-[120px] ">
            <div
              className="text-[15px] cursor-pointer  pt-1 pb-1"
              onClick={() => {
                setOpenModal(true);
              }}
            >
              <Bug>Báo cáo</Bug>
            </div>
          </div>
        </div>
      </div>
      <div className="my-8 border border-solid border-[#d5e0d5] rounded-[16px]">
        <article>
          <header className="p-8 border-b border-solid border-[#d5e0d5] flex items-center justify-between">
            {loading ? (
              <Skeleton className="w-full h-7" />
            ) : (
              <h3 className="text-2xl font-medium">{post?.title || ""}</h3>
            )}
          </header>

          <div className="p-8 pb-0">
            <h3 className="text-lg font-medium mb-2">Mô tả ngắn</h3>
            <div className="flex items-center justify-between">
              {loading ? (
                <Skeleton className="w-full h-4" />
              ) : (
                <div
                  style={{ padding: 20 }}
                  dangerouslySetInnerHTML={{ __html: post?.desc || "" }}
                />
              )}
            </div>
          </div>

          <div className="p-8 pb-0">
            <h3 className="text-lg font-medium mb-2">Nội dung</h3>
            <div className="flex items-center justify-between">
              {loading ? (
                <Skeleton className="w-full h-20" />
              ) : (
                <>
                  <div
                    style={{ padding: 20 }}
                    dangerouslySetInnerHTML={{ __html: post?.content || "" }}
                  />
                </>
              )}
            </div>
          </div>

          <div className="p-8">
            <div className="mb-6">
              <h3 className="text-lg font-medium mb-2">Kỹ năng</h3>
              <div className="flex items-center gap-x-3">
                {loading ? (
                  <>
                    <Skeleton className="w-20 h-7" />
                    <Skeleton className="w-20 h-7" />
                    <Skeleton className="w-20 h-7" />
                  </>
                ) : (
                  post?.skills.map((s) => (
                    <div
                      key={`selected-skill-${s.skill_id}`}
                      className="cursor-pointer flex items-center gap-x-1 border-2 border-solid border-transparent px-3 rounded-2xl h-8 text-sm font-medium leading-[31px] bg-[#108a00] hover:bg-[#14a800] text-white"
                      onClick={() => {}}
                    >
                      {s.skill_name}
                    </div>
                  ))
                )}
              </div>
            </div>
            <div className="mb-6 flex items-center">
              <h3 className="text-lg font-medium ">Thời hạn công việc</h3>
              <div className="flex items-center gap-x-3 pl-3">
                {loading ? (
                  <Skeleton className="h-[20px] w-[250px]" />
                ) : (
                  <p>
                    {post?.deadline &&
                      format(post.deadline || "", "dd-MM-yyyy")}
                  </p>
                )}
              </div>
            </div>
            <div className="mb-6 flex items-center">
              <h3 className="text-lg font-medium min-w-[130px]">
                File đính kèm
              </h3>
              <div className="flex items-center gap-x-3 pl-3">
                <div>
                  {!loading && post?.content_file && (
                    <Link href={post?.content_file} target="_blank">
                      <div className="upload-file">
                        <div className="flex px-3 py-3">
                          <div className="upload-file-thumbnail !p-0 w-8 h-8">
                            {
                              <FileIcon>
                                <VscFile />
                              </FileIcon>
                            }
                          </div>
                          {/* <div className='upload-file-info min-h-[2rem]'>
                            <h6 className='upload-file-name'>
                              {`${post?.content_file}`}
                            </h6>
                          </div> */}
                        </div>
                      </div>
                    </Link>
                  )}
                </div>
              </div>
            </div>
            <div className="mb-6 flex items-start">
              <h3 className="text-lg font-medium mb-2 min-w-[130px]">
                Hình ảnh
              </h3>
              <div className="flex items-center pl-3">
                <div>
                  {!loading && post?.thumbnail && (
                    <Link href={post?.thumbnail} target="_blank">
                      <div className="w-[200px] h-[100px] relative">
                        <Image
                          src={post?.thumbnail}
                          alt=""
                          className="object-contain w-[100%]"
                          fill
                        />
                      </div>
                    </Link>
                  )}
                </div>
              </div>
            </div>
          </div>
          {(post?.status?.toString() === "1" ||
            post?.status?.toString() === "2") && (
            <div className="mb-6 p-[20px] flex items-center flex-col">
              {post.applied?.length > 0 && (
                <>
                  <h3 className="text-lg font-medium mb-2 min-w-[130px]">
                    Số lượng ứng viên đã ứng tuyển: {post?.applied?.length || 0}
                  </h3>
                  <div className="flex items-center gap-x-3 pl-3 w-full">
                    <AppliedTable appliedList={post?.applied || []} />
                  </div>
                </>
              )}
              {post.list_invite?.length > 0 && (
                <>
                  <h3 className="text-lg font-medium mb-2 min-w-[130px]">
                    Số lượng ứng viên được mời {post?.list_invite?.length || 0}
                  </h3>
                  <div className="flex items-center gap-x-3 pl-3 w-full">
                    <InviteTable inviteList={post?.list_invite || []} />
                  </div>
                </>
              )}
            </div>
          )}

          <footer className="p-8 border-t border-solid border-[#d5e0d5] flex items-center justify-end">
            {!isCheckHaveApplied ? (
              <Button className="cursor-pointer flex items-center border-solid border-transparent text-sm font-medium  bg-[#108a00] hover:bg-[#14a800] text-white mr-4 ">
                <Link
                  href={{
                    pathname: `/freelancer/job/${postId}/apply`,
                  }}
                >
                  Ứng tuyển ngay
                </Link>
              </Button>
            ) : (
              <Button
                disabled
                className="cursor-pointer flex items-center border-solid border-transparent text-sm font-medium  bg-[#108a00] hover:bg-[#14a800] text-white mr-4 "
              >
                {/* <Link
                href={{
                  pathname: `/freelancer/job/${postId}/apply`,
                }}
              > */}
                Bạn đã ứng tuyển
                {/* </Link> */}
              </Button>
            )}
            <Button className="cursor-pointer flex items-center border-2 border-solid-[#14a800] border-transparent text-sm font-medium  bg-[#108a00] hover:bg-[#14a800] text-white mr-4">
              <Link
                href={`/freelancer/apply/${postId}`}
                className="flex gap-x-3"
              >
                <SquarePen color="#fff" className="w-5 h-5" />
                <span>Lưu công việc</span>
              </Link>
            </Button>
          </footer>
        </article>
      </div>
      <ReportForm
        isOpen={isOpenModel}
        onCancel={() => {
          setOpenModal(false);
        }}
        onReport={handleReport}
      />
    </section>
  );
};

export default PostDetail;
