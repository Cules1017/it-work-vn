import { Button } from "@/app/components/ui/button";
import {
  Tooltip,
  TooltipContent,
  TooltipProvider,
  TooltipTrigger,
} from "@/app/components/ui/tooltip";
import { Applied } from "@/app/types/client.types";
import { Eye, File, PenTool } from "lucide-react";
import { useContext, useState } from "react";
import AppliedInfoDialog from "./dialog/AppliedInfoDialog";
import { Table, TableProps, Tag } from "antd";
import { AuthContext } from "@/app/providers/AuthProvider";
import Link from "next/link";

interface IAppliedTable {
  appliedList: Applied[];
}

const AppliedTable: React.FC<IAppliedTable> = ({ appliedList = [] }) => {
  const [appliedInfo, setAppliedInfo] = useState<Applied | null>(null);
  const [infoUserApply, setInfoUserApply] = useState<Applied | null>(null);
  const { user } = useContext(AuthContext);

  const columns: TableProps<Applied>["columns"] = [
    {
      title: "STT",
      dataIndex: "id",
      key: "id",
      render: (text, record, index) => <a>{index + 1}</a>,
    },
    {
      title: "Tên ứng viên",
      dataIndex: "username",
      key: "username",
    },
    {
      title: "Email",
      dataIndex: "email",
      key: "email",
      width: 80,
    },

    {
      title: "Thư giới thiệu",
      dataIndex: "cover_letter",
      key: "cover_letter",
      width: 300,
    },
    // {
    //   title: "File đính kèm",
    //   dataIndex: "attachment_url",
    //   key: "attachment_url",
    // },
    {
      title: "File đính kèm",
      dataIndex: "attachment_url",
      key: "attachment_url",
      width: 100,
      render: (text, record) => (
        <div className="flex items-center gap-x-8">
          <TooltipProvider>
            <Tooltip>
              <TooltipTrigger asChild>
                <File
                  role="button"
                  className="w-5 h-5"
                  onClick={() => {
                    window.location.href = text;
                  }}
                />
              </TooltipTrigger>
              <TooltipContent>
                <p>Xem file đính kèm</p>
              </TooltipContent>
            </Tooltip>
          </TooltipProvider>
        </div>
      ),
    },
    {
      title: "Xem thông tin ứng viên",
      dataIndex: "id",
      key: "id",
      render: (text, record) => (
        // <Link href={`/client/applied/${record.id}`}>
        //   <a>Xem thông tin ứng viên</a>
        // </Link>
        <TooltipProvider>
          <Tooltip>
            <TooltipTrigger asChild>
              <Eye
                role="button"
                className="w-5 h-5"
                onClick={() => setAppliedInfo(record)}
              />
            </TooltipTrigger>
            <TooltipContent>
              <p>Xem thông tin ứng viên</p>
            </TooltipContent>
          </Tooltip>
        </TooltipProvider>
      ),
    },
    {
      title: "Xem thông tin ứng viên",
      dataIndex: "freelancer_id",
      key: "freelancer_id",
      render: (text, record) => {
        return <>{user&&text==user.id?<Button
          asChild
          variant="default"
          className="text-white bg-primary-color hover:bg-primary-color"
        >
          <Link
            href={`/info-contract/${record.job_id}`}
          >
            Hợp đồng
          </Link>
        </Button>:<></>}</>
      
    }}
  ];

  return (
    <>
      <Table className="w-[100%]" columns={columns} dataSource={appliedList} />
      {appliedInfo && (
        <AppliedInfoDialog
          info={appliedInfo}
          onClose={() => setAppliedInfo(null)}
        />
      )}
      {/* {infoUserApply && (
        <CreateContractDialog
          info={infoUserApply}
          onClose={() => setInfoUserApply(null)}
        />
      )} */}
    </>
  );
};

export default AppliedTable;
