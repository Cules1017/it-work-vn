import { IMenu } from '@/app/types/menu.types';

const findTalentSubMenu: IMenu[] = [
  {
    title: 'Tất cả các công việc',
    href: '/dashboard',
    description: 'Danh sách các list công việc',
  },
  {
    title: 'Tạo công việc mới',
    href: '/post/create',
    description: 'Tạo một công việc mới',
  },
  {
    title: 'Công việc đang làm',
    href: '/my-job',
    description: 'Sub 3 description',
  },
];

const findWorkSubMenu: IMenu[] = [
  {
    title: 'Tất cả hợp đồng',
    href: '/contracts',
    description: 'Những hợp đồng đang làm.',
  },
  {
    title: 'Chờ ký',
    href: '/contracts?completed=3',
    description: 'Những hợp đồng đang làm.',
  },
  {
    title: 'Hợp đồng đang làm',
    href: '/contracts?completed=0',
    description: 'Những hợp đồng đang làm.',
  },
  {
    title: 'Hợp đồng đã hoàn tất',
    href: '/contracts?completed=1',
    description: 'Những hợp đồng đã hoàn tất.',
  },
  {
    title: 'Hợp đồng đã bị hủy',
    href: '/contracts?completed=2',// 4 và 5
    description: 'Những hợp đồng đã bị hủy.',
  },
  // {
  //     title: 'Win work with ads',
  //     href: '/docs/find-work/win-work-with-ads',
  //     description: 'Get noticed by the right client.',
  // },
  // {
  //     title: 'Join Freelancer Plus',
  //     href: '/docs/find-work/scroll-area',
  //     description: 'Access more Connects, get stragetic insight on competitors, and try out the latest tools.',
  // },
];
const manageFreelancerSubMenu: IMenu[] = [
  {
    title: 'Danh sách freelancer đề xuất',
    href: '/client/invite-freelancer',
    description: 'Danh sách freelancer đề xuất.',
  },
  {
    title: 'Đánh giá freelancer',
    href: '/my-job',
    description: 'Xem và quản lý công việc đang làm.',
  },
];
const manageInvite: IMenu[] = [
  {
    title: 'Lời mời làm việc',
    href: '/freelancer/accept-invite',
    description: 'Lời mời làm việc.',
  },
];
const manageReport: IMenu[] = [
  {
    title: 'Công việc trong tháng',
    href: '/report-job-month',
    description: 'Báo cáo công việc trong tháng',
  },

  {
    title: 'Công việc trong năm',
    href: '/report-job-year',
    description: 'Báo cáo ông việc trong năm',
  },
];
export {
  findTalentSubMenu,
  findWorkSubMenu,
  manageFreelancerSubMenu,
  manageInvite,
  manageReport,
};
