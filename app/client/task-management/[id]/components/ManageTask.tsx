"use client";

import { freelancerServices } from "@/app/services/freelancer.services";
import { AppliedJob } from "@/app/types/freelancer.type";
import { DetailClientPost } from "@/app/types/client.types";
import React, { useCallback, useContext, useEffect, useState } from "react";
import TaskBoardProvider from "./TaskBoard/TaskBoardContext";
import TaskBoard from "./TaskBoard";
import LoadingTaskPage from "../loading";
import { Button, Modal } from "antd";
import { smartContractService } from "@/app/services/thirbWeb.services";
import { useContract } from "@thirdweb-dev/react";
import { appConfig } from "@/app/configs/app.config";
import { ReloadIcon } from "@radix-ui/react-icons";
import { useRouter } from "next/navigation";
import { NotificationContext } from "@/app/providers/NotificationProvider";
import { useStateContext } from "@/context";
import { clientServices } from "@/app/services/client.services";
import { commonServices } from "@/app/services/common.services";

interface IManageTask {
  id: string;
}
const ManageTask: React.FC<IManageTask> = ({ id }) => {
  const [loading, setLoading] = useState(false);
  const [post, setPost] = useState<DetailClientPost | null>(null);
  const [jobs, setJobs] = useState<AppliedJob[]>([]);
  const [infoContract, setInfoContract] = useState<any>(null);
  const { openNotificationWithIcon } = useContext(NotificationContext);
  const [loadingConfirmCompleteJob, setLoadingConfirmCompleteJob] =
    useState(false);
  const [loadingConfirmRejectJob, setLoadingConfirmRejectJob] = useState(false);
  const [openModalReview, setOpenModalReview] = useState(false);
  const router = useRouter();
  const { address, connect } = useStateContext();
  // const { contract } = useContract(appConfig.contractId);
  const { contract } = useContract(appConfig.contractId);
  useEffect(() => {
    const fetchFreelancerJobs = async () => {
      const res = await freelancerServices.getAppliedJobs({});
      setJobs(res?.data?.data);
    };
    fetchFreelancerJobs();
  }, []);

  const sendNotification = async (data: any) => {
    try {
      // setIsGettingPosts(true);
      const res = await commonServices.sendNotication(data);
      if (res.status === 200) {
        console.log("send notification success", res);
      }
    } catch (error) {
      console.log("error", error);
    } finally {
      // setIsGettingPosts(false);
    }
  };

  useEffect(() => {
    const getContractDetail = async () => {
      if (contract) {
        const data = await smartContractService.getDetailContractByJobId(
          parseInt(id),
          contract
        );
        setInfoContract(data);
        if (data?.status == 2) setOpenModalReview(true);
        if (data?.status === 3) router.push(`/client/job/${id}/reviewing-job`);
      }
    };
    getContractDetail();
  }, [contract]);

  useEffect(() => {
    const fetchPostData = async (jobId: string) => {
      try {
        setLoading(true);
        const res = await freelancerServices.getPost(jobId);
        if (res.data) {
          setPost(res.data);
        }
      } catch (error) {
      } finally {
        setLoading(false);
      }
    };
    if (id) {
      fetchPostData(id);
    }
  }, [id]);

  const currentStatusJob = (jobs || []).find(
    (j) => j.job_id?.toString() === id
  );

  // Xử lý hoàn thành hợp đồng
  const handleCompletedJob = useCallback(() => {
    const checkData = async () => {
      try {
        setLoadingConfirmCompleteJob(true);

        const data = await contract?.call("finalizeContract", [
          infoContract?.contract_id,
        ]);

        if (data != undefined) {
          const res = await clientServices.completeContract(parseInt(id));

          openNotificationWithIcon(
            "success",
            "Thành công",
            "Xác nhận công việc thành công."
          );
          sendNotification({
            title: "Thông báo",
            message: `Hợp đồng làm việc đã được xác nhận hoàn thành 😂`,
            linkable: `/info-contract/${id}`,
            smail: 1,
            imagefile: null,
            user_type: "freelancer",
            user_id: post?.nominee?.freelancer_id,
          });
          router.push(`/info-contract/${id}`);
        } else {
          openNotificationWithIcon(
            "error",
            "Thất bại",
            "Xác nhận công việc thành công thất bại"
          );
        }
      } catch (error) {
        openNotificationWithIcon(
          "error",
          "Thất bại",
          "Xác nhận công việc thành công thất bại"
        );
      } finally {
        setLoadingConfirmCompleteJob(false);
      }
    };

    if (address) {
      console.log("address", address);
      checkData();
    } else {
      connect();
    }
  }, [contract, id]);

  //reject cônng viêc
  const handleRejectJob = useCallback(() => {
    const checkData = async () => {
      try {
        setLoadingConfirmRejectJob(true);
        const data = await contract?.call("rejectCompletion", [
          infoContract?.contract_id,
        ]);
        console.log("data", data);
        if (data != undefined) {
          openNotificationWithIcon(
            "success",
            "Thành công",
            "Reject việc thành công."
          );
          sendNotification({
            title: `Yêu cầu làm lại công việc này ${post?.title}`,
            message: `Xác nhận hoàn thành công việc của bạn bị từ chối😂`,
            linkable: `/info-contract/${id}`,
            smail: 1,
            imagefile: null,
            user_type: "freelancer",
            user_id: post?.nominee?.freelancer_id,
          });
          // router.push(`/info-contract/${id}`);
        } else {
          openNotificationWithIcon(
            "error",
            "Thất bại",
            "Xác nhận công việc thành công thất bại"
          );
        }
      } catch (error) {
        openNotificationWithIcon("error", "Thất bại", "Xác nhận  thất bại");
        // setLoading(false);
      } finally {
        setLoadingConfirmRejectJob(false);
      }
    };
    if (address) {
      console.log("address", address);
      checkData();
    } else {
      connect();
    }
  }, [contract, id]);

  return (
    <>
      {loading ? (
        <LoadingTaskPage />
      ) : (
        <div>
          {/* <div className="flex justify-between">
            <h2 className="text-4xl font-semibold -tracking-[1px]">
              Quản lý task - {post?.title}
            </h2>
          </div> */}
          <div className="flex justify-between">
            <div>
              <p className="text-3xl font-semibold -tracking-[1px]">
                Quản lý task - {post?.title}
              </p>
            </div>
            {/* <div className="">
              <Button
                type="primary"
                className="!bg-blue-500"
                size="large"
                onClick={handleCompletedJob}
              >
                {loadingConfirmCompleteJob && (
                  <ReloadIcon className="mr-2 h-4 w-4 animate-spin inline-flex" />
                )}
                {address
                  ? "Xác nhận hoàn thành"
                  : "Kết nối ví MetaMask để xác nhận hoàn thành"}
              </Button>
            </div> */}
            <div className="">
              {infoContract?.status === 2 ? (
                <>
                  <Button
                    type="primary"
                    className="!bg-blue-500 mr-2"
                    size="middle"
                    onClick={handleCompletedJob}
                  >
                    {loadingConfirmCompleteJob && (
                      <ReloadIcon className="mr-2 h-4 w-4 animate-spin inline-flex" />
                    )}
                    {address ? "Xác nhận hoàn thành" : "Kết nối MetaMask"}
                  </Button>
                  <Button
                    type="primary"
                    className="!bg-red-500"
                    size="middle"
                    onClick={handleRejectJob}
                  >
                    {loadingConfirmRejectJob && (
                      <ReloadIcon className="mr-2 h-4 w-4 animate-spin inline-flex" />
                    )}
                    {address ? "Yêu cầu làm lại" : "Kết nối MetaMask để reject"}
                  </Button>
                </>
              ) : infoContract?.status === 3 ? (
                <Button
                  type="primary"
                  className="!bg-green-500"
                  size="middle"
                  //  onClick={handleCompletedJob}
                >
                  Đã hoàn thành
                </Button>
              ) : null}
            </div>
          </div>
          <div className="my-8">
            <TaskBoardProvider>
              <TaskBoard jobId={id} />
            </TaskBoardProvider>
          </div>
        </div>
      )}
      <Modal
        title="Thông báo review công việc"
        centered
        open={openModalReview}
        onOk={() => setOpenModalReview(false)}
        onCancel={() => setOpenModalReview(false)}
        width={700}
      >
        {" "}
        <div
          style={{ padding: 20 }}
          dangerouslySetInnerHTML={{ __html: post?.desc || "" }}
        />
        <strong>
          Đã được freelancer báo cáo hoàn thành, bạn hãy review lại và xác nhận
          lần nữa
        </strong>
      </Modal>
    </>
  );
};

export default ManageTask;
