"use client";

import {
  SearchBarProvider,
} from "./context/SearchBarContext";

import Freelancers from "./components/Freelancers";
import Pagiantion from "./components/Pagination";

const ListFreelancer = () => {
  return (
    <>
   
    <SearchBarProvider>
      <div className="relative">
        {/* <SearchBar /> */}
        <Freelancers />
        <Pagiantion />
      </div>
    </SearchBarProvider>
    </>
   
  );
};

export default ListFreelancer;
