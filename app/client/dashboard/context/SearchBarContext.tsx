import { clientServices } from "@/app/services/client.services";
import { ClientPostList } from "@/app/types/client.types";
import constants from "@/app/utils/constants";
import { isEmpty } from "lodash";
import {
  Dispatch,
  SetStateAction,
  createContext,
  useEffect,
  useState,
} from "react";

interface ISearchBarContext {
  status: string;
  searchText: string;
  posts: ClientPostList;
  isGettingPosts: boolean;
  total: number;
  page: number;
  totalPage: number;
  skills: any[];
  setStatus?: Dispatch<SetStateAction<string>>;
  setSearchText?: Dispatch<SetStateAction<string>>;
  setPrice?: Dispatch<SetStateAction<string[]>>;
  // setStatus?: Dispatch<SetStateAction<string>>;
  setDates?: Dispatch<SetStateAction<string[]>>;
  setSkills?: Dispatch<SetStateAction<any[]>>;
  // setSearchText?: Dispatch<SetStateAction<string>>;
  handleGoPage?: (page: number) => void;
}
export const SearchBarContext = createContext<ISearchBarContext>({
  status: "-1",
  searchText: "",
  page: 1,
  posts: [],
  total: 0,
  totalPage: 0,
  isGettingPosts: false,
  skills: [],
});

export const SearchBarProvider = ({
  children,
}: {
  children: React.ReactNode;
}) => {
  const [price, setPrice] = useState([""]);
  const [statusOpts, setStatusOpts] = useState<string>("-1");
  const [posts, setPosts] = useState<ClientPostList>([]);
  const [isGettingPosts, setIsGettingPosts] = useState(true);
  const [page, setPage] = useState(1);
  const [dates, setDates] = useState([""]);
  const [skills, setSkills] = useState<any[]>([]);
  const [searchText, setSearchText] = useState("");
  const [total, setTotal] = useState(0);
  const [totalPage, setTotalPage] = useState(0);

  const handleGoPage = (page: number) => {
    setPage(page);
  };

  useEffect(() => {
    const fecthPosts = async (data: any) => {
     
      try {
        setIsGettingPosts(true);
        const res = await clientServices.getPosts({
          page: data?.page || 1,
          num: constants.PAGE_SIZE,
          ...data,
          status: data?.status === "-1" ? "" : data.status,
        });
        if (res.data && !isEmpty(res.data.data)) {
          setPosts(res.data.data);
          setTotal(res.data.total);
          setTotalPage(res.data.total_page);
        }
      } catch (error) {
        console.log("error", error);
      } finally {
        setIsGettingPosts(false);
      }
    };
    if (dates && skills && statusOpts && price && page) {
      const params = {
        page,
        num: constants.PAGE_SIZE,
        status: statusOpts,
        keyword: searchText || "",
        skills: skills?.map((s: any) => s.id)?.toString(),
        bids: price?.toString(),
        deadline: dates.toString(),
      };
   
      fecthPosts(params);
    }
  }, [skills, statusOpts, dates, price, page, searchText]);
  // }, [statusOpts, page, searchText]);

  return (
    <SearchBarContext.Provider
      value={{
        page,
        status: statusOpts,
        searchText,
        posts,
        isGettingPosts,
        total,
        totalPage,
        setStatus: setStatusOpts,
        setSearchText,
        handleGoPage,
        setPrice,
        setDates,
        setSkills,
        skills,
      }}
    >
      {children}
    </SearchBarContext.Provider>
  );
};
