"use client";
import React, { useCallback, useContext, useEffect, useState } from "react";
import { useForm, SubmitHandler, SubmitErrorHandler } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
// import { Textarea } from "@/app/components/ui/textarea";
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/app/components/ui/form";
import { Input, notification } from "antd";
import { Button } from "@/app/components/ui/button";
import { useContract } from "@thirdweb-dev/react";
import { useStateContext } from "@/context";
import { ReloadIcon } from "@radix-ui/react-icons";
import { commonServices } from "@/app/services/common.services";
import { AuthContext } from "@/app/providers/AuthProvider";
import { Nominee } from "@/app/types/client.types";
import { Button as ButtonAnt, Checkbox, Modal } from "antd";
import PolicyViews from "../../components/PolicyViews";
import SignaturePadSimple from "../../components/SignaturePad";
import constants from "@/app/utils/constants";
import InputOtp from "../../components/InputOtp";
import { smartContractService } from "@/app/services/thirbWeb.services";
import { useRouter } from "next/navigation";
import { appConfig } from "@/app/configs/app.config";
import { IconType } from "antd/es/notification/interface";
import { clientServices } from "@/app/services/client.services";
import { NotificationContext } from "@/app/providers/NotificationProvider";
// import Link from "next/link";
const Textarea = Input.TextArea;

const CancelFormContractSchema = yup.object({
  // title: yup.string().required(""),
  description: yup.string().required(""),
  // deadline: yup.date().required(),
  // bids: yup.number().required(),
  // signature: yup.string().required("Vui lòng nhập chữ ký của bạn"),
  // allowSendMail: yup.bool(),
});

export interface SignUpSubmitValue {}

interface ICancelFormContract {
  handleCancelAccount: (data: SignUpSubmitValue) => void;
}
interface ICancelContract {
  nominee: Nominee;
}

const CancelFormContract: React.FC<ICancelContract> = ({ nominee }) => {
  const [loading, setLoading] = useState(false);
  const user = useContext(AuthContext);
  const [imgSignature, setImgSignature] = useState<String>("");
  const [acceptedPolicy, setAcceptedPolicy] = useState<boolean>(false);
  const [checked, setChecked] = useState(false);
  const [disabledPolicy, setDisabledPolicy] = useState(true);
  const [open, setOpen] = useState(false);
  const { contract } = useContract(appConfig.contractId);
  const { address, connect } = useStateContext();
  const router = useRouter();
  const [verify, setVerify] = useState(false);
  const [infoContract, setInfoContract] = useState<any>(null);

 
  const onChange = (e: any) => {
    console.log("checked = ", e.target.checked);
    setChecked(e.target.checked);
  };
  const { openNotificationWithIcon } = useContext(NotificationContext);

  const form = useForm({
    resolver: yupResolver(CancelFormContractSchema),
    defaultValues: {
      description: "",
    },
  });

  const sendNotification = async (data: any) => {
    try {
      // setIsGettingPosts(true);
      const res = await commonServices.sendNotication(data);
      if (res.status === 200) {
        console.log("send notification success", res);
      }
    } catch (error) {
      console.log("error", error);
    } finally {
      // setIsGettingPosts(false);
    }
  };

  useEffect(() => {
    if (imgSignature) {
      if (!verify) {
        setOpen(true);
      } else {
        setOpen(false);
      }
    }
  }, [imgSignature, verify]);
  /////Các hàm MODAL/////

  const hideModal = () => {
    setOpen(false);
  };
  //get thông tin hợp đồng
  useEffect(() => {
    const getContractDetail = async () => {
      if (contract) {
        const data = await smartContractService.getDetailContractByJobId(
          Number(nominee.job_id), // Convert nominee.job_id to a number
          contract
        );
        setInfoContract(data);
        // if (data?.status == 2) setOpenModalReview(true);
        // if(data?.status === 3)  router.push(`/client/job/${id}/reviewing-job`);
      }
    };
    getContractDetail();
  }, [contract]);

  const onSubmit: SubmitHandler<any> = async (data) => {
    debugger
    if (address) {
      try {
        // console.log("data");
        setLoading(true);
       
        const responseContract = await contract?.call(
          "cancelContract",
          [
            infoContract?.contract_id,
            data.description,
          ],
        );
        if(responseContract != undefined){
          const res = await clientServices.cancelContract(Number(nominee.job_id));
          openNotificationWithIcon("success", "Thành công.", "Đã hủy hợp đồng thành công.");
          sendNotification({
            title: `${user.user?.username} đã hủy hợp đồng ${data.title}`,
            message: `${data.description}`,
            linkable: ``,
            smail: 1,
            imagefile: null,
            user_type: "freelancer",
            user_id: nominee.freelancer_id,
          });
        }else{
          openNotificationWithIcon("error", "Thất bại.", "Đã có lỗi xảy ra trong quá trình hủy hợp đồng.");
        }
      } catch (err) {
        openNotificationWithIcon("error", "Thất bại.", "Đã có lỗi xảy ra trong quá trình hủy hợp đồng.");
      }
      finally{
        setLoading(false);
      }
    } else {
      debugger
      connect();
    }
 
  };
  const onError: SubmitErrorHandler<SignUpSubmitValue> = (errors) => {
    console.log("error", errors);
  };

  return (
    <>
      <div className="">
        <div className="my-6">
          <h2 className="text-3xl -tracking-[1px] font-medium text-center">
            Quy định hủy hợp đồng
          </h2>
          <span>
            <p className="text-[#001e00] text-lg text-center">
              Hãy cân nhắc trước khi thực hiện việc hủy hợp đồng,
              việc này sẽ ảnh hưởng đến uy tín của bạn trong tương lai. Thêm vào
              đó, số tiền bạn đã đặt cọc sẽ không được hoàn lại mà nó sẽ được
              chuyển cho freelancer.
            </p>
          </span>
        </div>
        <Form {...form}>
          <form className="" onSubmit={form.handleSubmit(onSubmit, onError)}>
            <div className="grid grid-cols-1 gap-x-1">
              <FormField
                control={form.control}
                name="description"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel className="text-lg">
                      Hãy cho chúng tôi biết một vài lý do khiến bạn hủy hợp
                      đồng
                    </FormLabel>
                    <FormControl>
                      <Textarea rows={6} {...field} />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
            </div>
            <div className="grid grid-cols-1 gap-x-1 my-2">
              <PolicyViews setDisabledPolicy={setDisabledPolicy}></PolicyViews>
              <Checkbox
                checked={checked}
                disabled={disabledPolicy}
                onChange={onChange}
              >
                {disabledPolicy
                  ? "Vui lòng đọc điều khoảng trước khi tích vào đây"
                  : "Tôi đã đọc kỹ và tôi chấp nhận tất cả điều khoản nêu trên."}
              </Checkbox>
            </div>
           {checked ? (
              <>
                {/* <div className="grid grid-cols-1 gap-x-1 justify-items-center">
                  <FormItem>
                    <FormLabel>{"Chữ ký"}</FormLabel>
                    <SignaturePadSimple setImg={setImgSignature} />

                    <FormMessage />
                  </FormItem>
                </div> */}

                <div className="text-center mt-6">
                  {/* {verify ? ( */}
                    <Button
                      disabled={loading}
                      className="bg-button-primary hover:bg-button-primary/80 px-6 border-2 border-solid border-transparent rounded-[10rem] transition-all inline-flex justify-center items-center max-h-10 leading-[calc_2.5rem_-_1px] text-base font-medium disabled:bg-button-disabled disabled:text-[#9aaa97] disabled:!cursor-not-allowed disabled:pointer-events-auto"
                      onClick={() => {
                        form.handleSubmit(onSubmit, onError);
                      }}
                    >
                      {loading && (
                        <ReloadIcon className="mr-2 h-4 w-4 animate-spin inline-flex" />
                      )}
                      {address ? "Hủy hợp đồng" : "Kết nối ví MetaMask"}
                    </Button>
               
                </div>
              </>
            ) : (
              <></>
            )} 
          </form>
        </Form>
        {/* <Modal
          className="text-center"
          title="Nhập mã OTP, mã OTP đã được gởi về mail của bạn"
          open={open}
          onCancel={hideModal}
          footer={[]}
        >
          <InputOtp setVerify={setVerify}></InputOtp>
        </Modal> */}
      </div>
    </>
  );
};

export default CancelFormContract;
