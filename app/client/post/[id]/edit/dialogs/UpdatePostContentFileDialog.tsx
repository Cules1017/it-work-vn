import { Button } from "@/app/components/ui/button";
import {
  Dialog,
  DialogContent,
  DialogFooter,
  DialogHeader,
  DialogTitle,
} from "@/app/components/ui/dialog";
import { Label } from "@/app/components/ui/label";
import { useContext, useState } from "react";
import { useToast } from "@/app/components/ui/use-toast";
import { cn } from "@/lib/utils";
import { EditPostContext } from "../context/EditPostContext";
import { clientServices } from "@/app/services/client.services";
import Upload from "@/app/components/themes/Upload";
import { NotificationContext } from "@/app/providers/NotificationProvider";
import { ReloadIcon } from "@radix-ui/react-icons";

const UpdatePostContentFileDialog = () => {
  const { onCloseModal, post, handleGetPostDetail } =
    useContext(EditPostContext);
  const { toast } = useToast();
  const [file, setFile] = useState<File | null>(null);
  const { openNotificationWithIcon } = useContext(NotificationContext);
  const [loading, setLoading] = useState<boolean>(false);

  const handleSubmit = async () => {
    if (!post || !file) return;
    try {
      const res = await clientServices.updatePost({
        id: post.id,
       
        content_file: file,
      });
      if (res.data) {
        openNotificationWithIcon(
          "error",
          "Cập nhật thành công",
          "Thông tin bài viết đã được cập nhật"
        );
        setLoading(false);
        handleGetPostDetail?.(post.id?.toString());
        onCloseModal?.();
      }
    } catch (error) {
      console.log("error", error);
      openNotificationWithIcon(
        "error",
        "Thất bại",
        "Thông tin bài viết cập nhật thất bại!"
      );
      setLoading(false);
    }
  };

  return (
    <Dialog open={true} onOpenChange={() => onCloseModal?.()}>
      <DialogContent className="max-w-[60vw]">
        <DialogHeader>
          <DialogTitle>Cập nhật thông tin bài viết</DialogTitle>
        </DialogHeader>
        <div className="grid gap-4 py-4">
          <div className="grid grid-cols-4 items-center gap-4">
            <Label className="text-right">File nội dung công việc</Label>
            <div className="col-span-3">
              <Upload
                className="h-[66px]"
                multiple={false}
                disabled={file ? true : false}
                showList={true}
                fileList={file ? [file as File] : []}
                draggable={true}
                onChange={(file) => {
                  setFile(file[0]);
                }}
                onFileRemove={() => {
                  setFile(null);
                }}
              />
            </div>
          </div>
        </div>
        <DialogFooter>
          <Button
            type="button"
            className="bg-primary-color"
            onClick={() => onCloseModal?.()}
          >
            Đóng
          </Button>
          <Button
            disabled={loading}
            className="bg-primary-color hover:bg-[#108a00]/80"
            onClick={() => handleSubmit()}
          >
            {loading && (
              <ReloadIcon className="mr-2 h-4 w-4 animate-spin inline-flex" />
            )}
            Cập nhật
          </Button>
        </DialogFooter>
      </DialogContent>
    </Dialog>
  );
};

export default UpdatePostContentFileDialog;
