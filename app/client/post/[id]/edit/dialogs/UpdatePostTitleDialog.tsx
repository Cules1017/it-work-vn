import { Button } from "@/app/components/ui/button";
import {
  Dialog,
  DialogContent,
  DialogFooter,
  DialogHeader,
  DialogTitle,
} from "@/app/components/ui/dialog";
import { Label } from "@/app/components/ui/label";
import { useContext, useRef, useState } from "react";
import { useToast } from "@/app/components/ui/use-toast";
import { EditPostContext } from "../context/EditPostContext";
import { clientServices } from "@/app/services/client.services";
import { Input } from "@/app/components/ui/input";
import { NotificationContext } from "@/app/providers/NotificationProvider";
import { ReloadIcon } from "@radix-ui/react-icons";

const UpdatePostTitleDialog = () => {
  const { onCloseModal, post, handleGetPostDetail } =
    useContext(EditPostContext);
  const { toast } = useToast();
  const [loading, setLoading] = useState<boolean>(false);
  const titleRef = useRef<HTMLInputElement | null>(null);
  const { openNotificationWithIcon } = useContext(NotificationContext);

  const handleSubmit = async () => {
    if (!post) return;
    try {
      setLoading(true);
      const res = await clientServices.updatePost({
        id: post.id,
        title: titleRef.current?.value || post.title || "",
      });

      if (res.data) {
        // toast({
        //   title: "Cập nhật thành công",
        //   description: "Thông tin bài viết đã được cập nhật",
        //   className: cn(
        //     "top-0 right-0 flex fixed md:max-w-[420px] md:top-4 md:right-4"
        //   ),
        //   duration: 1000,
        // });
        openNotificationWithIcon(
          "success",
          "Cập nhật thành công",
          "Thông tin bài viết đã được cập nhật"
        );
        handleGetPostDetail?.(post.id?.toString());
        onCloseModal?.();
        setLoading(false);
      }
    } catch (error) {
      openNotificationWithIcon(
        "error",
        "Thất bại",
        "Thông tin bài viết cập nhật thất bại!"
      );
      setLoading(false);
    }
  };

  return (
    <Dialog open={true} onOpenChange={() => onCloseModal?.()}>
      <DialogContent>
        <DialogHeader>
          <DialogTitle>Cập nhật thông tin bài viết</DialogTitle>
        </DialogHeader>
        <div className="grid gap-4 py-4">
          <div className="grid grid-cols-4 items-center gap-4">
            <Label className="text-right">Tiêu đề công việc</Label>
            <Input
              className="col-span-3"
              ref={titleRef}
              defaultValue={post?.title}
            />
          </div>
        </div>
        <DialogFooter>
          <Button
            type="button"
            className="bg-primary-color"
            onClick={() => onCloseModal?.()}
          >
            Đóng
          </Button>
          <Button
            disabled={loading}
            className="bg-primary-color hover:bg-[#108a00]/80"
            onClick={() => handleSubmit()}
          >
            {loading && (
              <ReloadIcon className="mr-2 h-4 w-4 animate-spin inline-flex" />
            )}
            Cập nhật
          </Button>
        </DialogFooter>
      </DialogContent>
    </Dialog>
  );
};

export default UpdatePostTitleDialog;
