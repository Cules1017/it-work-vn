import { Button } from "@/app/components/ui/button";
import {
  Dialog,
  DialogContent,
  DialogFooter,
  DialogHeader,
  DialogTitle,
} from "@/app/components/ui/dialog";
import { Label } from "@/app/components/ui/label";
import { useCallback, useContext, useRef, useState } from "react";
import { useToast } from "@/app/components/ui/use-toast";
import { EditPostContext } from "../context/EditPostContext";
import { clientServices } from "@/app/services/client.services";
import { Editor } from "@tinymce/tinymce-react";
import { NotificationContext } from "@/app/providers/NotificationProvider";
import { ReloadIcon } from "@radix-ui/react-icons";
import { appConfig } from "@/app/configs/app.config";

const UpdatePostContentDialog = () => {
  const { onCloseModal, post, handleGetPostDetail } =
    useContext(EditPostContext);
  const contentRef = useRef<HTMLTextAreaElement | null>(null);
  const [content, setContent] = useState<string | HTMLTextAreaElement>("");
  const [loading, setLoading] = useState<boolean>(false);
  const { openNotificationWithIcon } = useContext(NotificationContext);
  const handleSubmit = useCallback(async () => {
    // console.log(content);
    //    = async () => {
    if (!post) return;
    try {
      const res = await clientServices.updatePost({
        id: post.id,
        content: content || contentRef.current?.value || post.content,
      });
      setLoading(true);
      if (res.data) {
        openNotificationWithIcon(
          "success",
          "Cập nhật thành công",
          "Thông tin bài viết đã được cập nhật"
        );
        handleGetPostDetail?.(post.id?.toString());
        setLoading(false);
        onCloseModal?.();
      }
    } catch (error) {
      openNotificationWithIcon(
        "error",
        "Thất bại",
        "Thông tin bài viết cập nhật thất bại!"
      );
    } finally {
      setLoading(false);
    }
    // };
  }, [content]);

  return (
    <Dialog open={true} onOpenChange={() => onCloseModal?.()}>
      <DialogContent className="max-w-[60vw]">
        <DialogHeader>
          <DialogTitle>Cập nhật thông tin bài viết</DialogTitle>
        </DialogHeader>
        <div className=" py-4">
          <div className="items-center">
            <Label className="text-right">Nội dung công việc</Label>
            {/* <Textarea
                            className='col-span-3'
                            ref={contentRef}
                            defaultValue={post?.content}
                        /> */}
            <Editor
              apiKey={appConfig.mceKey}
              init={{
                plugins:
                "anchor autolink charmap codesample emoticons image link lists media searchreplace table visualblocks wordcount checklist mediaembed casechange export formatpainter pageembed linkchecker a11ychecker tinymcespellchecker permanentpen powerpaste advtable advcode editimage advtemplate ai mentions tinycomments tableofcontents footnotes mergetags autocorrect typography inlinecss markdown",
              toolbar:
                "undo redo | blocks fontfamily fontsize | bold italic underline strikethrough | link image media table mergetags | addcomment showcomments | spellcheckdialog a11ycheck typography | align lineheight | checklist numlist bullist indent outdent | emoticons charmap | removeformat",
              tinycomments_mode: "embedded",
              tinycomments_author: "Author name",
                // mergetags_list: [
                //   { value: "First.Name", title: "First Name" },
                //   { value: "Email", title: "Email" },
                // ],
                ai_request: (request: any, respondWith: any) =>
                  respondWith.string(() =>
                    Promise.reject("See docs to implement AI Assistant")
                  ),
              }}
              //   className="col-span-3"
              //initialValue="Nhập thông tin giới thiệu về bạn! Nội dung này sẽ được hiển thị trên trang cá nhân của bạn."
              // onEditorChange={(content: string) =>
              //   form.setFieldsValue({ desc: content })
              // }
              initialValue={post?.content || ""}
              onEditorChange={(content: string) => {
                setContent(content);
                // form.setFieldsValue({ desc: content });
              }}
            />
          </div>
        </div>
        <DialogFooter>
          <Button
            type="button"
            className="bg-primary-color"
            onClick={() => onCloseModal?.()}
          >
            Đóng
          </Button>
          <Button
            disabled={loading}
            className="bg-primary-color hover:bg-[#108a00]/80"
            onClick={() => handleSubmit()}
          >
            {loading && (
              <ReloadIcon className="mr-2 h-4 w-4 animate-spin inline-flex" />
            )}
            Cập nhật
          </Button>
        </DialogFooter>
      </DialogContent>
    </Dialog>
  );
};

export default UpdatePostContentDialog;
