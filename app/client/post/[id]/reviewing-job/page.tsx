'use client';
import React from "react";
import PostDetailProvider from "../context/PostDetailProvider";
import DetailPage from "./components/DetailPage";
interface IReviewing {
  params: {
    id: string;
  };
}
const Reviewing: React.FC<IReviewing> = ({ params }) => {
  return (
    <PostDetailProvider>
      <DetailPage postId={params.id} />
    </PostDetailProvider>
  );
};

export default Reviewing;
