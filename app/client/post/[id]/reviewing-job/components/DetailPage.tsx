"use client";
import { Button, Result, Skeleton } from "antd";
import React, { useContext, useEffect } from "react";
import { DetailPostContext } from "../../context/PostDetailProvider";
import { useRouter } from "next/navigation";
// import { DetailPostContext } from "../../context";
interface IDetailPage {
  postId: string;
}
const DetailPage = ({ postId }: IDetailPage) => {
  const { post, loading, handleGetPostDetail } = useContext(DetailPostContext);
  // const searchParams = useSearchParams();
  const router = useRouter();
  useEffect(() => {
    if (postId) {
      handleGetPostDetail?.(postId);
    }
  }, [postId]);

  return loading ? (
    <>
      <Skeleton className="w-20 h-7" />
      <Skeleton className="w-20 h-7" />
      <Skeleton className="w-20 h-7" />
    </>
  ) : (
    post && (
      <section>
        <Result
          status="success"
          title={
            <>
              <p>Công việc ${post.title} này đã hoàn thành tốt đẹp!`</p>
              <p>
                Bạn có thể bấm nút về trang chi tiết để xem lại lịch sử công
                việc đã làm
              </p>
            </>
          }
          subTitle={
            <div
              // style={{ padding: 20 }}
              dangerouslySetInnerHTML={{ __html: post?.desc || "" }}
            />
          }
          extra={[
            <Button
              type="primary"
              className="!bg-blue-500"
              size="large"
              onClick={()=>{
                router.push(`/client/post/${post.id}`);
              }}
            >
              Đi về trang chi tiết công việc
            </Button>,
          ]}
        />
      </section>
    )
  );
};

export default DetailPage;
