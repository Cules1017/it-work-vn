import React, { useContext, useEffect } from "react";
import { DetailPostContext } from "../../context/PostDetailProvider";
import { Skeleton } from "@/app/components/ui/skeleton";
import CreateFormContract from "../../components/FormCreate";
import { useSearchParams } from "next/navigation";
import { Applied } from "@/app/types/client.types";
interface ICreateContract {
  postId: string;
}
const CreateContractDetail = ({ postId }: ICreateContract) => {
  const { post, loading, handleGetPostDetail } = useContext(DetailPostContext);
  const searchParams = useSearchParams();
  useEffect(() => {
    if (postId) {
      handleGetPostDetail?.(postId);
    }
  }, [postId]);
  // get freelancer from listApplied by freelancerId
  const freelancerId = searchParams.get("freelancerId");
  //If get nominee to create contract
  const isInvite = searchParams.get("isInvite");
  let freelancer;
  if(isInvite){
     freelancer = post?.list_invite[0]
  }else{
    freelancer = post?.applied?.find(
      (item) => item.freelancer_id === freelancerId
    );
  }
 

  return loading ? (
    <>
      <Skeleton className="w-20 h-7" />
      <Skeleton className="w-20 h-7" />
      <Skeleton className="w-20 h-7" />
    </>
  ) : (
    post && freelancer && (
      <section>
        <CreateFormContract freelancer={freelancer as Applied} />
      </section>
    )
  );
};

export default CreateContractDetail;
