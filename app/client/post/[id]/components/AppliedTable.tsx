import { Button } from "@/app/components/ui/button";

import {
  Tooltip,
  TooltipContent,
  TooltipProvider,
  TooltipTrigger,
} from "@/app/components/ui/tooltip";
import { Applied } from "@/app/types/client.types";
import { Eye, File, PenTool } from "lucide-react";
import { useState } from "react";
import AppliedInfoDialog from "./dialogs/AppliedInfoDialog";
import Link from "next/link";
import { Table, TableProps } from "antd";

interface IAppliedTable {
  appliedList: Applied[];
  signContract: { isSigner: boolean; contractInfo: any };
  listContract: any;
}
const AppliedTable: React.FC<IAppliedTable> = ({
  appliedList = [],
  signContract,
  listContract,
}) => {
  const [appliedInfo, setAppliedInfo] = useState<Applied | null>(null);
  console.log(
    "QUERIES",
    1 == parseInt(signContract?.contractInfo[9]) && signContract.isSigner,
    parseInt(signContract?.contractInfo[9]),
    signContract
  );
  // id: contract[0].toNumber(),
  // jobIdcurent: contract[1].toNumber(),
  // title: contract[2],
  // status: contract[3],
  // freelancerId: contract[4].toNumber(),
  // clientId: contract[5].toNumber(),
  const checkFreelancer = (id: any) => {
    console.log(id, listContract);
    //return 0;
    let result = 0;
    for (let index = 0; index < listContract.length; index++) {
      if (
        listContract[index].freelancerId == id &&
        listContract[index].status >= 4
      )
        return -1; // đã hủy
      if (
        listContract[index].freelancerId == id &&
        listContract[index].status == 0
      )
        return 1; //đã tạo chưa kí
      if (
        listContract[index].freelancerId == id &&
        listContract[index].status >= 1
      )
        return 2; //đã kí
    }
    return 0; /// chưa ai kí
  };
  const isContractWaiting = () => {
    console.log('listContract', listContract)
    return listContract.some(
      (contract: { status: number }) => contract.status === 0
    );
  };
  const isContractPendingSign = () => {
    return listContract.some(
      (contract: { status: number }) => contract.status === 0
    );
  }
  console.log('isContractPendingSign---', isContractPendingSign())


  const getIdContract = (id: any) => {
    console.log(id, listContract);
    let result = 0;
    for (let index = 0; index < listContract.length; index++) {
      if (listContract[index].freelancerId == id) return listContract[index].id;
    }
  };

  const columns: TableProps<Applied>["columns"] = [
    {
      title: "STT",
      dataIndex: "id",
      key: "id",
      render: (text, record, index) => <a>{index + 1}</a>,
    },
    {
      title: "Tên ứng viên",
      dataIndex: "username",
      key: "username",
    },
    {
      title: "Email",
      dataIndex: "email",
      key: "email",
      width: 80,
    },

    {
      title: "Thư giới thiệu",
      dataIndex: "cover_letter",
      key: "cover_letter",
      width: 300,
    },
    {
      title: "File đính kèm",
      dataIndex: "attachment_url",
      key: "attachment_url",
      width: 100,
      render: (text, record) => (
        <div className="flex items-center gap-x-8">
          <TooltipProvider>
            <Tooltip>
              <TooltipTrigger asChild>
                <File
                  role="button"
                  className="w-5 h-5"
                  onClick={() => {
                    window.location.href = text;
                  }}
                />
              </TooltipTrigger>
              <TooltipContent>
                <p>Xem file đính kèm</p>
              </TooltipContent>
            </Tooltip>
          </TooltipProvider>
        </div>
      ),
    },
    {
      title: "Ứng viên",
      dataIndex: "id",
      key: "id",
      render: (text, record) => {
        return (
          <div className="flex items-center justify-center gap-x-8">
            <TooltipProvider>
              <Tooltip>
                <TooltipTrigger asChild>
                  <Eye
                    role="button"
                    className="w-5 h-5"
                    onClick={() => setAppliedInfo(record)}
                  />
                </TooltipTrigger>
                <TooltipContent>
                  <p>Xem thông tin ứng viên</p>
                </TooltipContent>
              </Tooltip>
            </TooltipProvider>
          </div>
        );
      },
    },
    {
      title: "Thao tác",
      dataIndex: "freelancer_id",
      key: "freelancer_id",
      render: (text, record) => {
        console.log(record, checkFreelancer(text));

        return (
          <div className="flex items-center gap-x-8">
            {/* {!signContract.isSigner ? (
              <TooltipProvider>
                <Tooltip>
                  <TooltipTrigger asChild>
                    <Button
                      asChild
                      variant="default"
                      className="text-white bg-primary-color hover:bg-primary-color"
                    >
                      <Link
                        href={`/client/post/${record.job_id}/create-contract?freelancerId=${record.freelancer_id}`}
                      >
                        Ký hợp đồng
                      </Link>
                    </Button>
                  </TooltipTrigger>
                  <TooltipContent>
                    <p>Ký hợp đồng</p>
                  </TooltipContent>
                </Tooltip>
              </TooltipProvider>
            ) : (
              <></>
            )}
            {parseInt(record.freelancer_id) ==
            parseInt(signContract?.contractInfo[9]) ? (
              <TooltipProvider>
                <Tooltip>
                  <TooltipTrigger asChild>
                    <Button
                      asChild
                      variant="default"
                      className="text-white bg-primary-color hover:bg-primary-color"
                    >
                      <Link href={`/info-contract/${record.job_id}`}>
                        Xem hợp đồng
                      </Link>
                    </Button>
                  </TooltipTrigger>
                  <TooltipContent>
                    <p>Xem hợp đồng</p>
                  </TooltipContent>
                </Tooltip>
              </TooltipProvider>
            ) : (
              <></>
            )} */}
            {checkFreelancer(text) == 0 && !isContractPendingSign() ? (
              <TooltipProvider>
                <Tooltip>
                  <TooltipTrigger asChild>
                    <Button
                      asChild
                      variant="default"
                      className="text-white bg-primary-color hover:bg-primary-color"
                    >
                      <Link
                        href={`/client/post/${record.job_id}/create-contract?freelancerId=${record.freelancer_id}`}
                      >
                        Ký hợp đồng
                      </Link>
                    </Button>
                  </TooltipTrigger>
                  <TooltipContent>
                    <p>Ký hợp đồng</p>
                  </TooltipContent>
                </Tooltip>
              </TooltipProvider>
            ) : (
              <></>
            )}
            {checkFreelancer(text) == 1 ? (
              <TooltipProvider>
                <Tooltip>
                  <TooltipTrigger asChild>
                    <Button
                      asChild
                      variant="default"
                      className="text-white bg-blue-500 hover:bg-blue-600"
                    >
                      <Link href={`/info-contract/${record.job_id}`}>
                        Chờ ký
                      </Link>
                    </Button>
                  </TooltipTrigger>
                  <TooltipContent>
                    <p>Xem hợp đồng chờ freelancer ký</p>
                  </TooltipContent>
                </Tooltip>
              </TooltipProvider>
            ) : (
              <></>
            )}
            {checkFreelancer(text) == -1 ? (
              <TooltipProvider>
                <Tooltip>
                  <TooltipTrigger asChild>
                    <Button
                      asChild
                      variant="default"
                      className="text-white bg-red-500 hover:bg-red-600"
                    >
                      <Link
                        href={`/info-contract-detail/${getIdContract(text)}`}
                      >
                        Đã Hủy
                      </Link>
                    </Button>
                  </TooltipTrigger>
                  <TooltipContent>
                    <p>Xem hợp đồng </p>
                  </TooltipContent>
                </Tooltip>
              </TooltipProvider>
            ) : (
              <></>
            )}
            {checkFreelancer(text) == 2 ? (
              <TooltipProvider>
                <Tooltip>
                  <TooltipTrigger asChild>
                    <Button
                      asChild
                      variant="default"
                      className="text-white bg-primary-color hover:bg-primary-color"
                    >
                      <Link href={`/info-contract/${record.job_id}`}>
                        Đã Ký
                      </Link>
                    </Button>
                  </TooltipTrigger>
                  <TooltipContent>
                    <p>Xem hợp đồng </p>
                  </TooltipContent>
                </Tooltip>
              </TooltipProvider>
            ) : (
              <></>
            )}
          </div>
        );
      },
    },
  ];
  return (
    <>
      <Table className="w-[100%]" columns={columns} dataSource={appliedList} />
      {appliedInfo && (
        <AppliedInfoDialog
          info={appliedInfo}
          onClose={() => setAppliedInfo(null)}
        />
      )}
    </>
  );
};

export default AppliedTable;
