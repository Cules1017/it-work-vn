"use client";

import { Button } from "antd";
import { Skeleton } from "@/app/components/ui/skeleton";
import { clientServices } from "@/app/services/client.services";
import { DetailClientPost } from "@/app/types/client.types";
import { format } from "date-fns";
import { FileIcon, SquarePen } from "lucide-react";
import Image from "next/image";
import Link from "next/link";
import { useContext, useEffect, useState } from "react";
import { VscFile } from "react-icons/vsc";
import AppliedTable from "./AppliedTable";
import _, { isEmpty } from "lodash";
import InviteTable from "./InviteTable";
import { Modal, Spin } from "antd";
import { useStateContext } from "@/context";
import { appConfig } from "@/app/configs/app.config";
import { isSigner, useContract } from "@thirdweb-dev/react";
import { smartContractService } from "@/app/services/thirbWeb.services";
import TextArea from "antd/es/input/TextArea";
import { NotificationContext } from "@/app/providers/NotificationProvider";
import { sendNotification } from "@/app/utils/helpers";
interface IPostDetail {
  postId: string;
}

const PostDetail: React.FC<IPostDetail> = ({ postId }) => {
  const [loading, setLoading] = useState(false);
  const [post, setPost] = useState<DetailClientPost | null>(null);
  const { address, connect } = useStateContext();
  const { contract } = useContract(appConfig.contractId);
  const [allContract, setAllContract] = useState<any>([]);
  const [loadingUpdatePolicy, setLoadingUpdatePolicy] =
    useState<boolean>(false);
  const [isOpenedModal, setIsOpenedModal] = useState(false);
  // const [contractInfo, setContractInfo] = useState({
  //   contract_id: -1,
  //   title: "",
  //   description: "",
  //   signature_freelancer: "",
  //   signature_client: "",
  //   bids: "0",
  //   status: 0,
  //   address_client: 0,
  //   address_freelancer: 0,
  //   freelancer_id: 0,
  //   client_id: 0,
  // });
  const [signContract, setSignContract] = useState({
    isSigner: false,
    contractInfo: [],
  });
  let { openNotificationWithIcon } = useContext(NotificationContext);
  const getAllContracts = async (id: any) => {
    const data = await smartContractService.getContractsByJobId(id, contract);
    setAllContract(data);
  };
  // const getContractByJobId = async (id: any) => {
  //   const data = await smartContractService.getDetailContractByJobId(
  //     parseInt(id),
  //     contract
  //   );
  //   console.log("data------->", data);
  //   if (data) {
  //     setContractInfo(data);
  //   }
  // };
  useEffect(() => {
    const fetchPostData = async (postId: string) => {
      if (contract) {
        try {
          setLoading(true);
          const res = await clientServices.getPost(postId);
          const data = null;
          await getAllContracts(postId);

          if (res.data) {
            setPost(res.data);
          }
          // if (post?.status === 3) {
          // await getContractByJobId(postId);

          // }
          if (data) {
            console.log("DATA: ", data, parseInt(data[9]));
            setSignContract({ isSigner: true, contractInfo: data });
          }
        } catch (error) {
          console.log(error);
        } finally {
          setLoading(false);
          setIsOpenedModal(false);
        }
      }
    };

    if (postId) {
      fetchPostData(postId);
    }
  }, [postId, contract]);
  // const updatePolicySmartContract = async () => {
  //   if (address) {
  //     setLoadingUpdatePolicy(true);
  //     try {
  //       // const data = await smartContractService.updatePolicy(
  //       //   contractInfo.contract_id,
  //       //   contract,
  //       //   contractInfo.description
  //       // );
  //       if (data) {
  //         console.log("updatePolicySmartContract", data);
  //       }
  //       sendNotification({
  //         title: `Đã có sự thay đổi trong điều khoản hợp đồng ${post?.title} 😍`,
  //         message: 'Những thay đổi trong điều khoản hợp đồng đang chờ xem xét',
  //         linkable: `/freelancer/my-job/${contractInfo?.contract_id}`,
  //         smail: 1,
  //         imagefile: null,
  //         user_type: "freelancer",
  //         user_id: contractInfo?.freelancer_id,
  //       });
  //       openNotificationWithIcon(
  //         "success",
  //         "Thành công",
  //         "Hợp đồng của bạn đã được cập nhật lại thành công."
  //       );
  //     } catch (error) {
  //       openNotificationWithIcon(
  //         "error",
  //         "Thất bại",
  //         "Hợp đồng của bạn đã được cập nhật lại thất bại."
  //       );
  //     } finally {
  //       setLoadingUpdatePolicy(false);
  //     }
  //   } else {
  //     // setLoadingUpdatePolicy(false);
  //     connect();
  //   }
  // };

  return (
    <>
      <section className="px-20">
        {loading ? (
          <Spin fullscreen></Spin>
        ) : (
          <>
            <div>
              <h2 className="text-4xl font-semibold -tracking-[1px]">
                Chi tiết công việc
              </h2>
            </div>
            <div className="my-8 border border-solid border-[#d5e0d5] rounded-[16px]">
              <article>
                <header className="p-8 border-b border-solid border-[#d5e0d5] flex items-center justify-between">
                  {loading ? (
                    <Skeleton className="w-full h-7" />
                  ) : (
                    <>
                      <h3 className="text-2xl font-medium">
                        {post?.title || ""}
                      </h3>
                      {post?.status?.toString() !== "3" && (
                        <Button
                          size="large"
                          className="rounded-full p-2 bg-transparent hover:bg-transparent"
                        >
                          <Link href={`/client/post/${postId}/edit`}>
                            <SquarePen color="#000" className="w-5 h-5" />
                          </Link>
                        </Button>
                      )}
                    </>
                  )}
                </header>

                <div className="p-8 pb-0">
                  <h3 className="text-lg font-medium mb-2">Mô tả ngắn</h3>
                  <div className="flex items-center justify-between">
                    {loading ? (
                      <Skeleton className="w-full h-4" />
                    ) : (
                      <div
                        style={{ padding: 20 }}
                        dangerouslySetInnerHTML={{ __html: post?.desc || "" }}
                      />
                    )}
                  </div>
                </div>

                <div className="p-8 pb-0">
                  <h3 className="text-lg font-medium mb-2">Nội dung</h3>
                  <div className="flex items-center justify-between">
                    {loading ? (
                      <Skeleton className="w-full h-20" />
                    ) : (
                      <div
                        style={{ padding: 20 }}
                        dangerouslySetInnerHTML={{
                          __html: post?.content || "",
                        }}
                      />
                    )}
                  </div>
                </div>

                <div className="p-8">
                  <div className="mb-6">
                    <h3 className="text-lg font-medium mb-2">Kỹ năng</h3>
                    <div className="flex items-center gap-x-3">
                      {loading ? (
                        <>
                          <Skeleton className="w-20 h-7" />
                          <Skeleton className="w-20 h-7" />
                          <Skeleton className="w-20 h-7" />
                        </>
                      ) : (
                        post?.skills.map((s) => (
                          <div
                            key={`selected-skill-${s.skill_id}`}
                            className="cursor-pointer flex items-center gap-x-1 border-2 border-solid border-transparent px-3 rounded-2xl h-8 text-sm font-medium leading-[31px] bg-[#108a00] hover:bg-[#14a800] text-white"
                            onClick={() => {}}
                          >
                            {s.skill_name}
                          </div>
                        ))
                      )}
                    </div>
                  </div>
                  <div className="mb-6 flex items-center">
                    <h3 className="text-lg font-medium ">Thời hạn công việc</h3>
                    <div className="flex items-center gap-x-3 pl-3">
                      {loading ? (
                        <Skeleton className="h-[20px] w-[250px]" />
                      ) : (
                        <p>
                          {post?.deadline &&
                            format(post.deadline || "", "dd-MM-yyyy")}
                        </p>
                      )}
                    </div>
                  </div>
                  <div className="mb-6 flex items-center">
                    <h3 className="text-lg font-medium min-w-[130px]">
                      File đính kèm
                    </h3>
                    <div className="flex items-center gap-x-3 pl-3">
                      {!loading && post?.content_file && (
                        <Link href={post?.content_file}>
                          <div className="upload-file">
                            <div className="flex px-3 py-3">
                              <div className="upload-file-thumbnail !p-0 w-8 h-8">
                                {
                                  <FileIcon>
                                    <VscFile />
                                  </FileIcon>
                                }
                              </div>
                            </div>
                          </div>
                        </Link>
                      )}
                    </div>
                  </div>
                  <div className="mb-6 flex items-start">
                    <h3 className="text-lg font-medium mb-2 min-w-[130px]">
                      Hình ảnh
                    </h3>
                    <div className="flex items-center pl-3">
                      {!loading && post?.thumbnail && (
                        <Link href={post?.thumbnail} target="_blank">
                          <div className="w-[200px] h-[100px] relative">
                            <Image
                              src={post?.thumbnail}
                              alt=""
                              className="object-contain w-[100%] h-[100%]"
                              fill
                            />
                          </div>
                        </Link>
                      )}
                    </div>
                  </div>
                  {/* Nominal */}
                  {(post?.status?.toString() === "3" ||
                    post?.status?.toString() === "4") &&
                    !isEmpty(post?.nominee) && (
                      <div className="mb-6 items-start">
                        <h3 className="text-lg font-medium mb-2 min-w-[130px]">
                          Ứng viên đã kí hợp đồng:{" "}
                          {/* {post?.applied?.length || 0} */}
                        </h3>
                        <div className="w-full bg-white border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700">
                          {/* <h3>Name: {post?.nominee?.username}</h3> */}
                          <div className="p-6 bg-white border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700">
                            {/* <a href="#"> */}
                            <h5 className="mb-2 text-2xl font-semibold tracking-tight text-gray-900 dark:text-white">
                              Thông tin ứng viên
                            </h5>
                            {/* </a> */}
                            <p className="mb-3 font-normal text-gray-500 dark:text-gray-400">
                              <strong>Tên ứng viên:</strong>{" "}
                              {post?.nominee?.username}
                            </p>
                            <p className="mb-3 font-normal text-gray-500 dark:text-gray-400">
                              <strong>Email:</strong> {post?.nominee?.email}
                            </p>
                            <p className="mb-3 font-normal text-gray-500 dark:text-gray-400">
                              <strong>Thư giới thiệu:</strong>{" "}
                              {post?.nominee?.cover_letter}
                            </p>

                            {!loading &&
                              post?.nominee &&
                              post?.nominee?.attachment_url && (
                                <div className="mb-6 flex items-center">
                                  <h3 className="text-lg font-medium min-w-[130px]">
                                    File đính kèm
                                  </h3>
                                  <div className="flex items-center gap-x-3 pl-3">
                                    <Link
                                      href={post?.nominee.attachment_url}
                                      target="_blank"
                                    >
                                      <div className="upload-file">
                                        <div className="flex px-3 py-3">
                                          <div className="upload-file-thumbnail !p-0 w-8 h-8">
                                            {
                                              <FileIcon>
                                                <VscFile />
                                              </FileIcon>
                                            }
                                          </div>
                                          <div className="upload-file-info min-h-[2rem]">
                                            <h6 className="upload-file-name">
                                              {`${post?.nominee?.username}`}
                                            </h6>
                                          </div>
                                        </div>
                                      </div>
                                    </Link>
                                  </div>
                                </div>
                              )}
                            <div>
                              <Button className="text-sm font-medium text-center text-white bg-blue-700 rounded-lg hover:bg-blue-800">
                                <Link
                                  href={`/show-detail-info/freelancer/${post.nominee?.freelancer_id}`}
                                >
                                  Xem thông tin chi tiết
                                </Link>
                              </Button>
                            </div>
                          </div>
                        </div>
                      </div>
                    )}
                  <div className="mb-6 flex items-start justify-end">
                    {post?.status?.toString() === "3" && (
                      <div className="flex flex-row justify-end">
                        <Button
                          size="large"
                          className="cursor-pointer flex items-center border-solid border-transparent text-sm font-medium
                      bg-[#108a00] hover:bg-[#14a800] text-white mx-4"
                        >
                          <Link
                            href={{
                              pathname: `/client/task-management/${post?.id}`,
                            }}
                          >
                            Quản lý chi tiết công việc
                          </Link>
                        </Button>
                        {/* <Button
                          size="large"
                          className="cursor-pointer flex items-center border-solid border-transparent text-sm font-medium
                        bg-[#108a00] hover:bg-[#14a800] text-white mx-4"
                          onClick={() => setIsOpenedModal(true)}
                        >
                          Sửa điều khoản hợp đồng
                        </Button> */}
                        <Button
                          size="large"
                          className="cursor-pointer flex items-center border-solid border-transparent text-sm font-medium  bg-[#108a00] hover:bg-[#14a800] text-white mr-4 "
                        >
                          <Link
                            href={{
                              pathname: `/info-contract/${post?.id}`,
                            }}
                          >
                            Xem chi tiết hợp đồng
                          </Link>
                        </Button>
                        <Button
                        size="large"
                          className="cursor-pointer flex items-center border-solid border-transparent text-sm font-medium 
                       bg-[red] hover:bg-red-300 text-white mr-4"
                        >
                          <Link
                            href={{
                              pathname: `/client/post/${post?.id}/cancel-contract`,
                            }}
                          >
                            Hủy hợp đồng
                          </Link>
                        </Button>
                      </div>
                    )}
                  </div>
                  <div className="mb-6 flex items-start justify-end">
                    {post?.status?.toString() === "4" && (
                      <div className="flex flex-row justify-end">
                        <Button
                        size="large"
                          className="cursor-pointer flex items-center border-solid border-transparent text-sm font-medium
                    bg-[#108a00] hover:bg-[#14a800] text-white mx-4"
                        >
                          <Link
                            href={{
                              pathname: `/client/task-management/${post?.id}`,
                            }}
                          >
                            Xem chi tiết công việc đã làm
                          </Link>
                        </Button>
                        <Button className="cursor-pointer flex items-center border-solid border-transparent text-sm font-medium  bg-[#108a00] hover:bg-[#14a800] text-white mr-4 ">
                          <Link
                            href={{
                              pathname: `/info-contract/${post?.id}`,
                            }}
                          >
                            Xem chi tiết hợp đồng
                          </Link>
                        </Button>
                        <Button
                        size="large"
                          className="cursor-pointer flex items-center border-solid border-transparent text-sm font-medium 
                       bg-[#108a00] hover:bg-[#14a800] text-white mx-4"
                        >
                          <Link
                            href={{
                              pathname: `/danh-gia/${post.nominee?.freelancer_id}/${post.id}`,
                            }}
                          >
                            Đánh giá Freelancer
                          </Link>
                        </Button>
                      </div>
                    )}
                  </div>
                  {(post?.status?.toString() === "1" ||
                    post?.status?.toString() === "2") && (
                    <div className="mb-6 p-[20px] flex items-center flex-col">
                      {post.applied?.length > 0 && (
                        <>
                          <h3 className="text-lg font-medium mb-2 min-w-[130px]">
                            Số lượng ứng viên đã ứng tuyển:{" "}
                            {post?.applied?.length || 0}
                          </h3>
                          <div className="flex items-center gap-x-3 pl-3 w-full">
                            <AppliedTable
                              signContract={signContract}
                              appliedList={post?.applied || []}
                              listContract={allContract}
                            />
                          </div>
                        </>
                      )}
                      {post.list_invite?.length > 0 && (
                        <>
                          <h3 className="text-lg font-medium mb-2 min-w-[130px]">
                            Số lượng ứng viên được mời{" "}
                            {post?.list_invite?.length || 0}
                          </h3>
                          <div className="flex items-center gap-x-3 pl-3 w-full">
                            <InviteTable inviteList={post?.list_invite || []} />
                          </div>
                        </>
                      )}
                    </div>
                  )}
                </div>
              </article>
            </div>
          </>
        )}
      </section>
      {/* <Modal
        open={isOpenedModal}
        title="Chỉnh sửa điều khoản hợp đồng"
        width={800}
        // minHeight={300}
        className="min-h-[400px]"
        // onOk={() => {}}
        onCancel={() => {
          setIsOpenedModal(false);
        }}
        footer={[
          <Button
            className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4  mr-4"
            key="ok"
            onClick={() => updatePolicySmartContract()}
            loading={loadingUpdatePolicy}
          >
            {address ? "Lưu" : "Connect wallet"}
          </Button>,
          <Button
            key="cancel"
            className="bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4"
            onClick={() => {
              setIsOpenedModal(false);
            }}
          >
            Đóng
          </Button>,
        ]}
      >
        <div>
          <TextArea
            defaultValue={contractInfo.description}
            placeholder="Nhập nội dung"
            autoSize={{ minRows: 5, maxRows: 10 }}
            onChange={(e) => {
              setContractInfo({
                ...contractInfo,
                description: e.target.value,
              });
            }}
          />
        </div>
      </Modal> */}
    </>
  );
};

export default PostDetail;
